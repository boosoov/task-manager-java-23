package com.rencredit.jschool.boruak.taskmanager.unit.locator;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class ServiceLocatorTest {

    @Test
    public void serviceLocatorTest() {
        @NotNull final IServiceLocator serviceLocator = new ServiceLocator();

        Assert.assertNotNull(serviceLocator.getCommandService());
        Assert.assertNotNull(serviceLocator.getInfoService());
        Assert.assertNotNull(serviceLocator.getSystemObjectService());
    }

}
