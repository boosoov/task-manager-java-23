package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ISystemObjectRepository;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.SystemObjectRepository;
import com.rencredit.jschool.boruak.taskmanager.service.SystemObjectService;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class SystemObjectServiceTest {

    @NotNull ISystemObjectRepository systemObjectRepository;
    @NotNull SystemObjectService systemObjectService;

    @Before
    public void init() {
        systemObjectRepository = new SystemObjectRepository();
        systemObjectService = new SystemObjectService(systemObjectRepository);
    }

    @Test(expected = EmptySessionException.class)
    public void testNegativePutSessionWithoutSession() throws EmptySessionException {
        systemObjectService.putSession(null);
    }

    @Test
    public void testPutSession() throws EmptySessionException {
        @NotNull final Session session = new Session();
        Assert.assertNull(systemObjectService.putSession( session));
        Assert.assertEquals(session, systemObjectService.putSession(session));
    }

    @Test
    public void testNegativeGetSessionWithoutSessionInBase() {
        Assert.assertNull(systemObjectService.getSession());
    }

    @Test
    public void testGetSession() throws EmptySessionException {
        @NotNull final Session session = new Session();
        systemObjectService.putSession( session);
        Assert.assertEquals(session, systemObjectService.getSession());
    }

    @Test
    public void testRemoveSession() throws EmptySessionException {
        @NotNull final Session session = new Session();
        systemObjectService.putSession( session);
        systemObjectService.removeSession();
        Assert.assertNull(systemObjectService.getSession());
    }

    @Test
    public void testClearAll() throws EmptySessionException {
        @NotNull final Session session = new Session();
        systemObjectService.putSession( session);
        systemObjectService.clearAll();
        Assert.assertNull(systemObjectService.getSession());
    }

}
