package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

@Category(IntegrationWithServerTestCategory.class)
public class ProjectEndpointTest {

    @NotNull IEndpointLocator endpointLocator;
    @NotNull ProjectEndpoint projectEndpoint;
    @NotNull SessionEndpoint sessionEndpoint;
    @NotNull AuthEndpoint authEndpoint;
    @NotNull UserEndpoint userEndpoint;
    @NotNull Session session;


    @Before
    public void init() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, BusyLoginException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        endpointLocator = new EndpointLocator();
        projectEndpoint = endpointLocator.getProjectEndpoint();
        sessionEndpoint = endpointLocator.getSessionEndpoint();
        authEndpoint = endpointLocator.getAuthEndpoint();
        userEndpoint = endpointLocator.getUserEndpoint();

        session = sessionEndpoint.openSession("admin", "admin");
        projectEndpoint.clearAllProject(session);
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception {
        projectEndpoint.clearAllProject(session);
        sessionEndpoint.closeSession(session);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeCreateProjectNameWithoutSession() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(null, "project01");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeCreateProjectNameWithoutName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, null);
    }

    @Test
    public void testCreateProjectName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, "project01");
        @NotNull Project project = projectEndpoint.findProjectByName(session, "project01");
        Assert.assertNotNull(project);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeCreateProjectNameDescriptionWithoutSession() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.createProjectNameDescription(null, "project02", "description");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeCreateProjectNameDescriptionWithoutName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.createProjectNameDescription(session, null, "description");
    }

    @Test(expected = EmptyDescriptionException_Exception.class)
    public void testNegativeCreateProjectNameWithoutDescription() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.createProjectNameDescription(session, "project02", null);
    }

    @Test
    public void testCreateProjectNameDescription() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.createProjectNameDescription(session, "project02", "description");
        @NotNull Project project = projectEndpoint.findProjectByName(session, "project02");
        Assert.assertNotNull(project);
        Assert.assertEquals("description", project.getDescription());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindAllProjectByUserIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.findAllProjectByUserId(null);
    }

    @Test
    public void testFindAllProjectByUserId() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, "project04");
        List<Project> projects = projectEndpoint.findAllProjectByUserId(session);
        Assert.assertFalse(projects.isEmpty());

        projectEndpoint.clearAllUserProjects(session);
        List<Project> projectsEmpty = projectEndpoint.findAllProjectByUserId(session);
        Assert.assertTrue(projectsEmpty.isEmpty());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeClearAllUserProjectsByUserIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.clearAllUserProjects(null);
    }

    @Test
    public void testClearAllUserProjectsByUserId() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, "project05");
        List<Project> projects = projectEndpoint.findAllProjectByUserId(session);
        Assert.assertFalse(projects.isEmpty());

        projectEndpoint.clearAllUserProjects(session);
        List<Project> projectsEmpty = projectEndpoint.findAllProjectByUserId(session);
        Assert.assertTrue(projectsEmpty.isEmpty());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindProjectByIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        projectEndpoint.findProjectById(null, "project06");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeFindProjectByIdWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        projectEndpoint.findProjectById(session, null);
    }

    @Test
    public void testFindProjectById() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        projectEndpoint.createProjectName(session, "project06");
        @NotNull Project project = projectEndpoint.findProjectByName(session, "project06");
        Assert.assertNotNull(project);
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectFromEndpoint);
        Assert.assertEquals("project06", projectFromEndpoint.getName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindProjectByIndexWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        projectEndpoint.findProjectByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException_Exception.class)
    public void testNegativeFindProjectByIndexWithoutIndex() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        projectEndpoint.findProjectByIndex(session, null);
    }

    @Test
    public void testFindProjectByIndex() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        projectEndpoint.createProjectName(session, "project06");
        @NotNull Project project = projectEndpoint.findProjectByName(session, "project06");
        Assert.assertNotNull(project);
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectByIndex(session, 0);
        Assert.assertNotNull(projectFromEndpoint);
        Assert.assertEquals("project06", projectFromEndpoint.getName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindProjectByNameWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        projectEndpoint.findProjectByName(null, "project07");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeFindProjectByNameWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        projectEndpoint.findProjectByName(session, null);
    }

    @Test
    public void testFindProjectByName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, "project07");
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectByName(session, "project07");
        Assert.assertEquals("project07", projectFromEndpoint.getName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveProjectByIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        projectEndpoint.removeProjectById(null, "id");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeRemoveProjectByIdWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception,  EmptyIdException_Exception {
        projectEndpoint.removeProjectById(session, null);
    }

    @Test
    public void testRemoveProjectById() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        projectEndpoint.createProjectName(session, "project08");
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNotNull(projectFromEndpoint);

        projectEndpoint.removeProjectById(session, projectFromEndpoint.getId());
        projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNull(projectFromEndpoint);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveProjectByIndexWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        projectEndpoint.removeProjectByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException_Exception.class)
    public void testNegativeRemoveProjectByIndexWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception,  IncorrectIndexException_Exception {
        projectEndpoint.removeProjectByIndex(session, null);
    }

    @Test
    public void testRemoveProjectByIndex() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        projectEndpoint.createProjectName(session, "project08");
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNotNull(projectFromEndpoint);

        projectEndpoint.removeProjectByIndex(session, 0);
        projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNull(projectFromEndpoint);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveProjectByNameWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        projectEndpoint.removeProjectByName(null, "project08");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeRemoveProjectByNameWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        projectEndpoint.removeProjectByName(session, null);
    }

    @Test
    public void testRemoveProjectByName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, "project08");
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNotNull(projectFromEndpoint);
        projectEndpoint.removeProjectByName(session, "project08");
        projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNull(projectFromEndpoint);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeUpdateProjectByIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectById(null, "id", "name", "description");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeUpdateProjectByIdWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectById(session, null, "name", "description");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeUpdateProjectByIdWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectById(session, "id", null, "description");
    }

    @Test(expected = EmptyDescriptionException_Exception.class)
    public void testNegativeUpdateProjectByIdWithoutDescription() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectById(session, "id", "name", null);
    }

    @Test
    public void testUpdateProjectById() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyProjectException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.createProjectName(session, "project08");
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNotNull(projectFromEndpoint);
        projectEndpoint.updateProjectById(session, projectFromEndpoint.getId(), "nameNew", "descriptionNew");
        projectFromEndpoint = projectEndpoint.findProjectByName(session, "nameNew");
        Assert.assertNotNull(projectFromEndpoint);
        Assert.assertEquals("descriptionNew", projectFromEndpoint.getDescription());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeUpdateProjectByIndexWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException_Exception.class)
    public void testNegativeUpdateProjectByIndexWithoutIndex() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectByIndex(session, null, "name", "description");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeUpdateProjectByIndexWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectByIndex(session, 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException_Exception.class)
    public void testNegativeUpdateProjectByIndexWithoutDescription() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyProjectException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.updateProjectByIndex(session, 0, "name", null);
    }

    @Test
    public void testUpdateProjectByIndex() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyProjectException_Exception, IncorrectIndexException_Exception, EmptyDescriptionException_Exception {
        projectEndpoint.createProjectName(session, "project08");
        @NotNull Project projectFromEndpoint = projectEndpoint.findProjectByName(session, "project08");
        Assert.assertNotNull(projectFromEndpoint);
        projectEndpoint.updateProjectByIndex(session, 0, "nameNew", "descriptionNew");
        projectFromEndpoint = projectEndpoint.findProjectByName(session, "nameNew");
        Assert.assertNotNull(projectFromEndpoint);
        Assert.assertEquals("descriptionNew", projectFromEndpoint.getDescription());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeClearAllProjectWithoutSession() throws DeniedAccessException_Exception {
        projectEndpoint.clearAllProject(null);
    }

    @Test
    public void testClearAllProject() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, "project01");
        projectEndpoint.createProjectName(session, "project02");
        projectEndpoint.createProjectName(session, "project03");
        List<Project> projects = projectEndpoint.getListProject(session);
        Assert.assertEquals(3, projects.size());
        projectEndpoint.clearAllProject(session);
        projects = projectEndpoint.getListProject(session);
        Assert.assertTrue(projects.isEmpty());
    }


    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeGetListProjectWithoutSession() throws DeniedAccessException_Exception {
        projectEndpoint.clearAllProject(null);
    }

    @Test
    public void testGetListProject() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        projectEndpoint.createProjectName(session, "project01");
        projectEndpoint.createProjectName(session, "project02");
        projectEndpoint.createProjectName(session, "project03");
        List<Project> projects = projectEndpoint.getListProject(session);
        Assert.assertEquals(3, projects.size());
    }

}
