package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

@Category(IntegrationWithServerTestCategory.class)
public class TaskEndpointTest {

    @NotNull IEndpointLocator endpointLocator;
    @NotNull TaskEndpoint taskEndpoint;
    @NotNull SessionEndpoint sessionEndpoint;
    @NotNull UserEndpoint userEndpoint;
    @NotNull AuthEndpoint authEndpoint;
    @NotNull Session session;

    @Before
    public void init() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, BusyLoginException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        endpointLocator = new EndpointLocator();
        taskEndpoint = endpointLocator.getTaskEndpoint();
        sessionEndpoint = endpointLocator.getSessionEndpoint();
        userEndpoint = endpointLocator.getUserEndpoint();
        authEndpoint = endpointLocator.getAuthEndpoint();

        session = sessionEndpoint.openSession("admin", "admin");

        taskEndpoint.clearAllTask(session);
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception {
        taskEndpoint.clearAllTask(session);
        sessionEndpoint.closeSession(session);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeCreateTaskNameWithoutSession() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(null, "task01");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeCreateTaskNameWithoutName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, null);
    }

    @Test
    public void testCreateTaskName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, "task01");
        @NotNull Task task = taskEndpoint.findTaskByName(session, "task01");
        Assert.assertNotNull(task);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeCreateTaskNameDescriptionWithoutSession() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.createTaskNameDescription(null, "task02", "description");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeCreateTaskNameDescriptionWithoutName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.createTaskNameDescription(session, null, "description");
    }

    @Test(expected = EmptyDescriptionException_Exception.class)
    public void testNegativeCreateTaskNameWithoutDescription() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.createTaskNameDescription(session, "task02", null);
    }

    @Test
    public void testCreateTaskNameDescription() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.createTaskNameDescription(session, "task02", "description");
        @NotNull Task task = taskEndpoint.findTaskByName(session, "task02");
        Assert.assertNotNull(task);
        Assert.assertEquals("description", task.getDescription());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindAllTaskByUserIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.findAllTaskByUserId(null);
    }

    @Test
    public void testFindAllTaskByUserId() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, "task04");
        List<Task> tasks = taskEndpoint.findAllTaskByUserId(session);
        Assert.assertFalse(tasks.isEmpty());

        taskEndpoint.removeAllUserTasks(session);
        List<Task> tasksEmpty = taskEndpoint.findAllTaskByUserId(session);
        Assert.assertTrue(tasksEmpty.isEmpty());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeClearAllUserTasksByUserIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.removeAllUserTasks(null);
    }

    @Test
    public void testClearAllUserTasksByUserId() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, "task05");
        List<Task> tasks = taskEndpoint.findAllTaskByUserId(session);
        Assert.assertFalse(tasks.isEmpty());

        taskEndpoint.removeAllUserTasks(session);
        List<Task> tasksEmpty = taskEndpoint.findAllTaskByUserId(session);
        Assert.assertTrue(tasksEmpty.isEmpty());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindTaskByIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        taskEndpoint.findTaskById(null, "task06");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeFindTaskByIdWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        taskEndpoint.findTaskById(session, null);
    }

    @Test
    public void testFindTaskById() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        taskEndpoint.createTaskName(session, "task06");
        @NotNull Task task = taskEndpoint.findTaskByName(session, "task06");
        Assert.assertNotNull(task);
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskById(session, task.getId());
        Assert.assertNotNull(taskFromEndpoint);
        Assert.assertEquals("task06", taskFromEndpoint.getName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindTaskByIndexWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        taskEndpoint.findTaskByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException_Exception.class)
    public void testNegativeFindTaskByIndexWithoutIndex() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        taskEndpoint.findTaskByIndex(session, null);
    }

    @Test
    public void testFindTaskByIndex() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        taskEndpoint.createTaskName(session, "task06");
        @NotNull Task task = taskEndpoint.findTaskByName(session, "task06");
        Assert.assertNotNull(task);
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskByIndex(session, 0);
        Assert.assertNotNull(taskFromEndpoint);
        Assert.assertEquals("task06", taskFromEndpoint.getName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeFindTaskByNameWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        taskEndpoint.findTaskByName(null, "task07");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeFindTaskByNameWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        taskEndpoint.findTaskByName(session, null);
    }

    @Test
    public void testFindTaskByName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, "task07");
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskByName(session, "task07");
        Assert.assertEquals("task07", taskFromEndpoint.getName());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveTaskByIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        taskEndpoint.removeTaskById(null, "id");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeRemoveTaskByIdWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception,  EmptyIdException_Exception {
        taskEndpoint.removeTaskById(session, null);
    }

    @Test
    public void testRemoveTaskById() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        taskEndpoint.createTaskName(session, "task08");
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNotNull(taskFromEndpoint);

        taskEndpoint.removeTaskById(session, taskFromEndpoint.getId());
        taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNull(taskFromEndpoint);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveTaskByIndexWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        taskEndpoint.removeTaskByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException_Exception.class)
    public void testNegativeRemoveTaskByIndexWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception,  IncorrectIndexException_Exception {
        taskEndpoint.removeTaskByIndex(session, null);
    }

    @Test
    public void testRemoveTaskByIndex() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception {
        taskEndpoint.createTaskName(session, "task08");
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNotNull(taskFromEndpoint);

        taskEndpoint.removeTaskByIndex(session, 0);
        taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNull(taskFromEndpoint);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeRemoveTaskByNameWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        taskEndpoint.removeTaskByName(null, "task08");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeRemoveTaskByNameWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception {
        taskEndpoint.removeTaskByName(session, null);
    }

    @Test
    public void testRemoveTaskByName() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, "task08");
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNotNull(taskFromEndpoint);
        taskEndpoint.removeTaskByName(session, "task08");
        taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNull(taskFromEndpoint);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeUpdateTaskByIdWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskById(null, "id", "name", "description");
    }

    @Test(expected = EmptyIdException_Exception.class)
    public void testNegativeUpdateTaskByIdWithoutId() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskById(session, null, "name", "description");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeUpdateTaskByIdWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskById(session, "id", null, "description");
    }

    @Test(expected = EmptyDescriptionException_Exception.class)
    public void testNegativeUpdateTaskByIdWithoutDescription() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskById(session, "id", "name", null);
    }

    @Test
    public void testUpdateTaskById() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyTaskException_Exception, EmptyIdException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.createTaskName(session, "task08");
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNotNull(taskFromEndpoint);
        taskEndpoint.updateTaskById(session, taskFromEndpoint.getId(), "nameNew", "descriptionNew");
        taskFromEndpoint = taskEndpoint.findTaskByName(session, "nameNew");
        Assert.assertNotNull(taskFromEndpoint);
        Assert.assertEquals("descriptionNew", taskFromEndpoint.getDescription());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeUpdateTaskByIndexWithoutSession() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException_Exception.class)
    public void testNegativeUpdateTaskByIndexWithoutIndex() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskByIndex(session, null, "name", "description");
    }

    @Test(expected = EmptyNameException_Exception.class)
    public void testNegativeUpdateTaskByIndexWithoutName() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskByIndex(session, 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException_Exception.class)
    public void testNegativeUpdateTaskByIndexWithoutDescription() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, IncorrectIndexException_Exception, EmptyNameException_Exception, EmptyTaskException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.updateTaskByIndex(session, 0, "name", null);
    }

    @Test
    public void testUpdateTaskByIndex() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyTaskException_Exception, IncorrectIndexException_Exception, EmptyDescriptionException_Exception {
        taskEndpoint.createTaskName(session, "task08");
        @NotNull Task taskFromEndpoint = taskEndpoint.findTaskByName(session, "task08");
        Assert.assertNotNull(taskFromEndpoint);
        taskEndpoint.updateTaskByIndex(session, 0, "nameNew", "descriptionNew");
        taskFromEndpoint = taskEndpoint.findTaskByName(session, "nameNew");
        Assert.assertNotNull(taskFromEndpoint);
        Assert.assertEquals("descriptionNew", taskFromEndpoint.getDescription());
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeClearAllTaskWithoutSession() throws DeniedAccessException_Exception {
        taskEndpoint.clearAllTask(null);
    }

    @Test
    public void testClearAllTask() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, "task01");
        taskEndpoint.createTaskName(session, "task02");
        taskEndpoint.createTaskName(session, "task03");
        List<Task> tasks = taskEndpoint.getListTask(session);
        Assert.assertEquals(3, tasks.size());
        taskEndpoint.clearAllTask(session);
        tasks = taskEndpoint.getListTask(session);
        Assert.assertTrue(tasks.isEmpty());
    }


    @Test(expected = DeniedAccessException_Exception.class)
    public void testNegativeGetListTaskWithoutSession() throws DeniedAccessException_Exception {
        taskEndpoint.clearAllTask(null);
    }

    @Test
    public void testGetListTask() throws EmptyNameException_Exception, DeniedAccessException_Exception, EmptyUserIdException_Exception {
        taskEndpoint.createTaskName(session, "task01");
        taskEndpoint.createTaskName(session, "task02");
        taskEndpoint.createTaskName(session, "task03");
        List<Task> tasks = taskEndpoint.getListTask(session);
        Assert.assertEquals(3, tasks.size());
    }

}
