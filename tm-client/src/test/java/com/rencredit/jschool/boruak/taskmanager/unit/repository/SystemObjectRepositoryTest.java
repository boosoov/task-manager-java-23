package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.SystemObjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class SystemObjectRepositoryTest {

    @Test
    public void testPutSession() {
        @NotNull final SystemObjectRepository systemObjectRepository = new SystemObjectRepository();
        @NotNull final Session session = new Session();
        @Nullable final Session savedSession = systemObjectRepository.putSession(session);
        Assert.assertNull(savedSession);
        @Nullable final Session oldSessionFromRepository = systemObjectRepository.putSession(session);
        Assert.assertEquals(session, oldSessionFromRepository);

    }

    @Test
    public void testGetSession() {
        @NotNull final SystemObjectRepository systemObjectRepository = new SystemObjectRepository();
        @NotNull final Session session = new Session();
        systemObjectRepository.putSession(session);
        @Nullable final Session sessionFromRepository = systemObjectRepository.getSession();
        Assert.assertEquals(session, sessionFromRepository);

    }

    @Test
    public void testRemoveSession() {
        @NotNull final SystemObjectRepository systemObjectRepository = new SystemObjectRepository();
        @NotNull final Session session = new Session();
        systemObjectRepository.putSession(session);
        systemObjectRepository.removeSession();
        @Nullable final Session sessionFromRepository = systemObjectRepository.getSession();
        Assert.assertNull(sessionFromRepository);

    }

    @Test
    public void testClearAll() {
        @NotNull final SystemObjectRepository systemObjectRepository = new SystemObjectRepository();
        @NotNull final Session session = new Session();
        systemObjectRepository.putSession(session);
        systemObjectRepository.clearAll();
        @Nullable final Session sessionFromRepository = systemObjectRepository.getSession();
        Assert.assertNull(sessionFromRepository);

    }

}
