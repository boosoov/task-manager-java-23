package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;
import java.util.List;

@Category(IntegrationWithServerTestCategory.class)
public class AuthEndpointTest {

    @NotNull IEndpointLocator endpointLocator;
    @NotNull AuthEndpoint authEndpoint;
    @NotNull UserEndpoint userEndpoint;
    @NotNull SessionEndpoint sessionEndpoint;
    @NotNull Session session;

    @Before
    public void init() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, BusyLoginException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        endpointLocator = new EndpointLocator();
        authEndpoint = endpointLocator.getAuthEndpoint();
        userEndpoint = endpointLocator.getUserEndpoint();
        sessionEndpoint = endpointLocator.getSessionEndpoint();

        session = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception {
        sessionEndpoint.closeSession(session);
    }

    @Test
    public void testLogIn() throws EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyHashLineException_Exception, DeniedAccessException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception {
        @NotNull final Session sessionTest = authEndpoint.logIn("test", "test");
        Assert.assertNotNull(sessionTest);
    }

    @Test
    public void testLogOut() throws DeniedAccessException_Exception {
        @NotNull final Result result = authEndpoint.logOut(session);
        Assert.assertTrue(result.isSuccess());
    }

    @Test
    public void testGetUserId() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        @NotNull final String userId = authEndpoint.getUserId(session);
        @NotNull final User user = userEndpoint.getUserByLogin(session,"admin");
        Assert.assertEquals(user.getId(), userId);
    }

    @Test
    public void testCheckRoles() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        @NotNull final List<Role> rolesAdmin = Arrays.asList(Role.ADMIN);
        final boolean result = authEndpoint.checkRoles(session, rolesAdmin);
        Assert.assertTrue(result);
    }

    @Test(expected = DeniedAccessException_Exception.class)
    public void testCheckRolesNotCorrectRole() throws NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception {
        @NotNull final List<Role> rolesUser = Arrays.asList(Role.USER);
        final boolean result = authEndpoint.checkRoles(session, rolesUser);
        Assert.assertFalse(result);
    }

    @Test
    public void testRegistrationLoginPassword() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, BusyLoginException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        final boolean result = authEndpoint.registrationLoginPassword("login01", "password01");
        Assert.assertTrue(result);
        @NotNull User user = userEndpoint.getUserByLogin(session,"login01");
        Assert.assertNotNull(user);
        userEndpoint.removeUserByLogin(session,"login01");
        user = userEndpoint.getUserByLogin(session,"login01");
        Assert.assertNull(user);
    }

    @Test
    public void testRegistrationLoginPasswordFirstName() throws EmptyFirstNameException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyEmailException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        final boolean result = authEndpoint.registrationLoginPasswordFirstName("login02", "password02", "firstName");
        Assert.assertTrue(result);
        @NotNull User user = userEndpoint.getUserByLogin(session,"login02");
        Assert.assertNotNull(user);
        Assert.assertEquals("firstName", user.getFirstName());
        userEndpoint.removeUserByLogin(session,"login02");
        user = userEndpoint.getUserByLogin(session,"login02");
        Assert.assertNull(user);
    }

    @Test
    public void testRegistrationLoginPasswordRole() throws EmptyLoginException_Exception, DeniedAccessException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, EmptyIdException_Exception, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception {
        final boolean result = authEndpoint.registrationLoginPasswordRole(session, "login03", "password03", Role.USER);
        Assert.assertTrue(result);
        @NotNull User user = userEndpoint.getUserByLogin(session,"login03");
        Assert.assertNotNull(user);
        Assert.assertEquals(Role.USER, user.getRole());
        userEndpoint.removeUserByLogin(session,"login03");
        user = userEndpoint.getUserByLogin(session,"login03");
        Assert.assertNull(user);
    }

}
