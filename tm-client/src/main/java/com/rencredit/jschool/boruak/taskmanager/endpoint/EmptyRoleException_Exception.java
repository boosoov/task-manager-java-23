
package com.rencredit.jschool.boruak.taskmanager.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-17T19:09:43.196+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "EmptyRoleException", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/")
public class EmptyRoleException_Exception extends Exception {

    private com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyRoleException emptyRoleException;

    public EmptyRoleException_Exception() {
        super();
    }

    public EmptyRoleException_Exception(String message) {
        super(message);
    }

    public EmptyRoleException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public EmptyRoleException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyRoleException emptyRoleException) {
        super(message);
        this.emptyRoleException = emptyRoleException;
    }

    public EmptyRoleException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyRoleException emptyRoleException, java.lang.Throwable cause) {
        super(message, cause);
        this.emptyRoleException = emptyRoleException;
    }

    public com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyRoleException getFaultInfo() {
        return this.emptyRoleException;
    }
}
