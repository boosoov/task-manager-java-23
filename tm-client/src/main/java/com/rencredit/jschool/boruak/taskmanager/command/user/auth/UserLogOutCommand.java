package com.rencredit.jschool.boruak.taskmanager.command.user.auth;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.AuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.endpoint.DeniedAccessException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UserLogOutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout system.";
    }

    @Override
    public void execute() throws DeniedAccessException_Exception {
        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        @NotNull final Session session = serviceLocator.getSystemObjectService().getSession();
        authEndpoint.logOut(session);
        serviceLocator.getSystemObjectService().removeSession();
        System.out.println("Have been logout");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
