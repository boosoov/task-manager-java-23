package com.rencredit.jschool.boruak.taskmanager.command.user;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.util.ViewUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProfileShowCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "view-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show profile.";
    }

    @Override
    public void execute() throws DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException, EmptyUserIdException_Exception, NotExistUserException_Exception, EmptySessionException_Exception, EmptyRoleException_Exception {
        @NotNull final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();
        @NotNull final Session session = serviceLocator.getSystemObjectService().getSession();
        @Nullable final String userId = authEndpoint.getUserId(session);
        @NotNull final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();
        @Nullable final User user = userEndpoint.getUserById(session, userId);
        ViewUtil.showUser(user);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
