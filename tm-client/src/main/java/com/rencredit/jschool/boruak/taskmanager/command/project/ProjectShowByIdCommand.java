package com.rencredit.jschool.boruak.taskmanager.command.project;

import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.rencredit.jschool.boruak.taskmanager.util.TerminalUtil.showProject;

public class ProjectShowByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() throws DeniedAccessException_Exception, EmptyUserIdException_Exception, EmptyIdException_Exception, EmptyProjectException {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @NotNull final Session session = serviceLocator.getSystemObjectService().getSession();
        @Nullable final Project project = projectEndpoint.findProjectById(session, id);
        showProject(project);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
