package com.rencredit.jschool.boruak.taskmanager.util;

import com.rencredit.jschool.boruak.taskmanager.endpoint.Project;
import com.rencredit.jschool.boruak.taskmanager.endpoint.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String line = nextLine();
        return Integer.parseInt(line);
    }

    static void showProject(@Nullable final Project project) throws EmptyProjectException {
        if (project == null) throw new EmptyProjectException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
    }

    static void showTask(@Nullable final Task task) throws EmptyTaskException {
        if (task == null) throw new EmptyTaskException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
    }

}
