
package com.rencredit.jschool.boruak.taskmanager.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.rencredit.jschool.boruak.taskmanager.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BusyLoginException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "BusyLoginException");
    private final static QName _DeniedAccessException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "DeniedAccessException");
    private final static QName _EmptyEmailException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyEmailException");
    private final static QName _EmptyFirstNameException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyFirstNameException");
    private final static QName _EmptyHashLineException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyHashLineException");
    private final static QName _EmptyIdException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyIdException");
    private final static QName _EmptyLoginException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyLoginException");
    private final static QName _EmptyPasswordException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyPasswordException");
    private final static QName _EmptyRoleException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyRoleException");
    private final static QName _EmptySessionException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptySessionException");
    private final static QName _EmptyUserException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyUserException");
    private final static QName _EmptyUserIdException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "EmptyUserIdException");
    private final static QName _NotExistUserException_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "NotExistUserException");
    private final static QName _CheckRoles_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "checkRoles");
    private final static QName _CheckRolesResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "checkRolesResponse");
    private final static QName _GetUserId_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserId");
    private final static QName _GetUserIdResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "getUserIdResponse");
    private final static QName _LogIn_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logIn");
    private final static QName _LogInResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logInResponse");
    private final static QName _LogOut_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logOut");
    private final static QName _LogOutResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "logOutResponse");
    private final static QName _RegistrationLoginPassword_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPassword");
    private final static QName _RegistrationLoginPasswordFirstName_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordFirstName");
    private final static QName _RegistrationLoginPasswordFirstNameResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordFirstNameResponse");
    private final static QName _RegistrationLoginPasswordResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordResponse");
    private final static QName _RegistrationLoginPasswordRole_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordRole");
    private final static QName _RegistrationLoginPasswordRoleResponse_QNAME = new QName("http://endpoint.taskmanager.boruak.jschool.rencredit.com/", "registrationLoginPasswordRoleResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.rencredit.jschool.boruak.taskmanager.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BusyLoginException }
     * 
     */
    public BusyLoginException createBusyLoginException() {
        return new BusyLoginException();
    }

    /**
     * Create an instance of {@link DeniedAccessException }
     * 
     */
    public DeniedAccessException createDeniedAccessException() {
        return new DeniedAccessException();
    }

    /**
     * Create an instance of {@link EmptyEmailException }
     * 
     */
    public EmptyEmailException createEmptyEmailException() {
        return new EmptyEmailException();
    }

    /**
     * Create an instance of {@link EmptyFirstNameException }
     * 
     */
    public EmptyFirstNameException createEmptyFirstNameException() {
        return new EmptyFirstNameException();
    }

    /**
     * Create an instance of {@link EmptyHashLineException }
     * 
     */
    public EmptyHashLineException createEmptyHashLineException() {
        return new EmptyHashLineException();
    }

    /**
     * Create an instance of {@link EmptyIdException }
     * 
     */
    public EmptyIdException createEmptyIdException() {
        return new EmptyIdException();
    }

    /**
     * Create an instance of {@link EmptyLoginException }
     * 
     */
    public EmptyLoginException createEmptyLoginException() {
        return new EmptyLoginException();
    }

    /**
     * Create an instance of {@link EmptyPasswordException }
     * 
     */
    public EmptyPasswordException createEmptyPasswordException() {
        return new EmptyPasswordException();
    }

    /**
     * Create an instance of {@link EmptyRoleException }
     * 
     */
    public EmptyRoleException createEmptyRoleException() {
        return new EmptyRoleException();
    }

    /**
     * Create an instance of {@link EmptySessionException }
     * 
     */
    public EmptySessionException createEmptySessionException() {
        return new EmptySessionException();
    }

    /**
     * Create an instance of {@link EmptyUserException }
     * 
     */
    public EmptyUserException createEmptyUserException() {
        return new EmptyUserException();
    }

    /**
     * Create an instance of {@link EmptyUserIdException }
     * 
     */
    public EmptyUserIdException createEmptyUserIdException() {
        return new EmptyUserIdException();
    }

    /**
     * Create an instance of {@link NotExistUserException }
     * 
     */
    public NotExistUserException createNotExistUserException() {
        return new NotExistUserException();
    }

    /**
     * Create an instance of {@link CheckRoles }
     * 
     */
    public CheckRoles createCheckRoles() {
        return new CheckRoles();
    }

    /**
     * Create an instance of {@link CheckRolesResponse }
     * 
     */
    public CheckRolesResponse createCheckRolesResponse() {
        return new CheckRolesResponse();
    }

    /**
     * Create an instance of {@link GetUserId }
     * 
     */
    public GetUserId createGetUserId() {
        return new GetUserId();
    }

    /**
     * Create an instance of {@link GetUserIdResponse }
     * 
     */
    public GetUserIdResponse createGetUserIdResponse() {
        return new GetUserIdResponse();
    }

    /**
     * Create an instance of {@link LogIn }
     * 
     */
    public LogIn createLogIn() {
        return new LogIn();
    }

    /**
     * Create an instance of {@link LogInResponse }
     * 
     */
    public LogInResponse createLogInResponse() {
        return new LogInResponse();
    }

    /**
     * Create an instance of {@link LogOut }
     * 
     */
    public LogOut createLogOut() {
        return new LogOut();
    }

    /**
     * Create an instance of {@link LogOutResponse }
     * 
     */
    public LogOutResponse createLogOutResponse() {
        return new LogOutResponse();
    }

    /**
     * Create an instance of {@link RegistrationLoginPassword }
     * 
     */
    public RegistrationLoginPassword createRegistrationLoginPassword() {
        return new RegistrationLoginPassword();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordFirstName }
     * 
     */
    public RegistrationLoginPasswordFirstName createRegistrationLoginPasswordFirstName() {
        return new RegistrationLoginPasswordFirstName();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordFirstNameResponse }
     * 
     */
    public RegistrationLoginPasswordFirstNameResponse createRegistrationLoginPasswordFirstNameResponse() {
        return new RegistrationLoginPasswordFirstNameResponse();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordResponse }
     * 
     */
    public RegistrationLoginPasswordResponse createRegistrationLoginPasswordResponse() {
        return new RegistrationLoginPasswordResponse();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordRole }
     * 
     */
    public RegistrationLoginPasswordRole createRegistrationLoginPasswordRole() {
        return new RegistrationLoginPasswordRole();
    }

    /**
     * Create an instance of {@link RegistrationLoginPasswordRoleResponse }
     * 
     */
    public RegistrationLoginPasswordRoleResponse createRegistrationLoginPasswordRoleResponse() {
        return new RegistrationLoginPasswordRoleResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link AbstractEntity }
     * 
     */
    public AbstractEntity createAbstractEntity() {
        return new AbstractEntity();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusyLoginException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "BusyLoginException")
    public JAXBElement<BusyLoginException> createBusyLoginException(BusyLoginException value) {
        return new JAXBElement<BusyLoginException>(_BusyLoginException_QNAME, BusyLoginException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeniedAccessException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "DeniedAccessException")
    public JAXBElement<DeniedAccessException> createDeniedAccessException(DeniedAccessException value) {
        return new JAXBElement<DeniedAccessException>(_DeniedAccessException_QNAME, DeniedAccessException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyEmailException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyEmailException")
    public JAXBElement<EmptyEmailException> createEmptyEmailException(EmptyEmailException value) {
        return new JAXBElement<EmptyEmailException>(_EmptyEmailException_QNAME, EmptyEmailException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyFirstNameException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyFirstNameException")
    public JAXBElement<EmptyFirstNameException> createEmptyFirstNameException(EmptyFirstNameException value) {
        return new JAXBElement<EmptyFirstNameException>(_EmptyFirstNameException_QNAME, EmptyFirstNameException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyHashLineException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyHashLineException")
    public JAXBElement<EmptyHashLineException> createEmptyHashLineException(EmptyHashLineException value) {
        return new JAXBElement<EmptyHashLineException>(_EmptyHashLineException_QNAME, EmptyHashLineException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyIdException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyIdException")
    public JAXBElement<EmptyIdException> createEmptyIdException(EmptyIdException value) {
        return new JAXBElement<EmptyIdException>(_EmptyIdException_QNAME, EmptyIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyLoginException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyLoginException")
    public JAXBElement<EmptyLoginException> createEmptyLoginException(EmptyLoginException value) {
        return new JAXBElement<EmptyLoginException>(_EmptyLoginException_QNAME, EmptyLoginException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyPasswordException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyPasswordException")
    public JAXBElement<EmptyPasswordException> createEmptyPasswordException(EmptyPasswordException value) {
        return new JAXBElement<EmptyPasswordException>(_EmptyPasswordException_QNAME, EmptyPasswordException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyRoleException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyRoleException")
    public JAXBElement<EmptyRoleException> createEmptyRoleException(EmptyRoleException value) {
        return new JAXBElement<EmptyRoleException>(_EmptyRoleException_QNAME, EmptyRoleException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptySessionException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptySessionException")
    public JAXBElement<EmptySessionException> createEmptySessionException(EmptySessionException value) {
        return new JAXBElement<EmptySessionException>(_EmptySessionException_QNAME, EmptySessionException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyUserException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyUserException")
    public JAXBElement<EmptyUserException> createEmptyUserException(EmptyUserException value) {
        return new JAXBElement<EmptyUserException>(_EmptyUserException_QNAME, EmptyUserException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmptyUserIdException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "EmptyUserIdException")
    public JAXBElement<EmptyUserIdException> createEmptyUserIdException(EmptyUserIdException value) {
        return new JAXBElement<EmptyUserIdException>(_EmptyUserIdException_QNAME, EmptyUserIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotExistUserException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "NotExistUserException")
    public JAXBElement<NotExistUserException> createNotExistUserException(NotExistUserException value) {
        return new JAXBElement<NotExistUserException>(_NotExistUserException_QNAME, NotExistUserException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "checkRoles")
    public JAXBElement<CheckRoles> createCheckRoles(CheckRoles value) {
        return new JAXBElement<CheckRoles>(_CheckRoles_QNAME, CheckRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckRolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "checkRolesResponse")
    public JAXBElement<CheckRolesResponse> createCheckRolesResponse(CheckRolesResponse value) {
        return new JAXBElement<CheckRolesResponse>(_CheckRolesResponse_QNAME, CheckRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserId")
    public JAXBElement<GetUserId> createGetUserId(GetUserId value) {
        return new JAXBElement<GetUserId>(_GetUserId_QNAME, GetUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "getUserIdResponse")
    public JAXBElement<GetUserIdResponse> createGetUserIdResponse(GetUserIdResponse value) {
        return new JAXBElement<GetUserIdResponse>(_GetUserIdResponse_QNAME, GetUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogIn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logIn")
    public JAXBElement<LogIn> createLogIn(LogIn value) {
        return new JAXBElement<LogIn>(_LogIn_QNAME, LogIn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logInResponse")
    public JAXBElement<LogInResponse> createLogInResponse(LogInResponse value) {
        return new JAXBElement<LogInResponse>(_LogInResponse_QNAME, LogInResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogOut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logOut")
    public JAXBElement<LogOut> createLogOut(LogOut value) {
        return new JAXBElement<LogOut>(_LogOut_QNAME, LogOut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogOutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "logOutResponse")
    public JAXBElement<LogOutResponse> createLogOutResponse(LogOutResponse value) {
        return new JAXBElement<LogOutResponse>(_LogOutResponse_QNAME, LogOutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPassword")
    public JAXBElement<RegistrationLoginPassword> createRegistrationLoginPassword(RegistrationLoginPassword value) {
        return new JAXBElement<RegistrationLoginPassword>(_RegistrationLoginPassword_QNAME, RegistrationLoginPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordFirstName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordFirstName")
    public JAXBElement<RegistrationLoginPasswordFirstName> createRegistrationLoginPasswordFirstName(RegistrationLoginPasswordFirstName value) {
        return new JAXBElement<RegistrationLoginPasswordFirstName>(_RegistrationLoginPasswordFirstName_QNAME, RegistrationLoginPasswordFirstName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordFirstNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordFirstNameResponse")
    public JAXBElement<RegistrationLoginPasswordFirstNameResponse> createRegistrationLoginPasswordFirstNameResponse(RegistrationLoginPasswordFirstNameResponse value) {
        return new JAXBElement<RegistrationLoginPasswordFirstNameResponse>(_RegistrationLoginPasswordFirstNameResponse_QNAME, RegistrationLoginPasswordFirstNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordResponse")
    public JAXBElement<RegistrationLoginPasswordResponse> createRegistrationLoginPasswordResponse(RegistrationLoginPasswordResponse value) {
        return new JAXBElement<RegistrationLoginPasswordResponse>(_RegistrationLoginPasswordResponse_QNAME, RegistrationLoginPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordRole")
    public JAXBElement<RegistrationLoginPasswordRole> createRegistrationLoginPasswordRole(RegistrationLoginPasswordRole value) {
        return new JAXBElement<RegistrationLoginPasswordRole>(_RegistrationLoginPasswordRole_QNAME, RegistrationLoginPasswordRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationLoginPasswordRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "registrationLoginPasswordRoleResponse")
    public JAXBElement<RegistrationLoginPasswordRoleResponse> createRegistrationLoginPasswordRoleResponse(RegistrationLoginPasswordRoleResponse value) {
        return new JAXBElement<RegistrationLoginPasswordRoleResponse>(_RegistrationLoginPasswordRoleResponse_QNAME, RegistrationLoginPasswordRoleResponse.class, null, value);
    }

}
