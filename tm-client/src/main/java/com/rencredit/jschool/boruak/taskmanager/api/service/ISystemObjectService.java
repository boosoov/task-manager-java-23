package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import org.jetbrains.annotations.Nullable;

public interface ISystemObjectService {

    @Nullable
    Session putSession(@Nullable Session session) throws EmptySessionException;

    @Nullable
    Session getSession();

    @Nullable
    Session removeSession();

    void clearAll();

}
