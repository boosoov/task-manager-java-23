package com.rencredit.jschool.boruak.taskmanager.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-17T19:09:39.921+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/closeSessionAllRequest", output = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/closeSessionAllResponse", fault = {@FaultAction(className = DeniedAccessException_Exception.class, value = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/closeSessionAll/Fault/DeniedAccessException")})
    @RequestWrapper(localName = "closeSessionAll", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", className = "com.rencredit.jschool.boruak.taskmanager.endpoint.CloseSessionAll")
    @ResponseWrapper(localName = "closeSessionAllResponse", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", className = "com.rencredit.jschool.boruak.taskmanager.endpoint.CloseSessionAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.rencredit.jschool.boruak.taskmanager.endpoint.Result closeSessionAll(
        @WebParam(name = "session", targetNamespace = "")
        com.rencredit.jschool.boruak.taskmanager.endpoint.Session session
    ) throws DeniedAccessException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/openSessionRequest", output = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/openSessionResponse", fault = {@FaultAction(className = EmptyUserException_Exception.class, value = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/openSession/Fault/EmptyUserException"), @FaultAction(className = EmptyPasswordException_Exception.class, value = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/openSession/Fault/EmptyPasswordException"), @FaultAction(className = EmptyHashLineException_Exception.class, value = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/openSession/Fault/EmptyHashLineException"), @FaultAction(className = EmptyLoginException_Exception.class, value = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/openSession/Fault/EmptyLoginException"), @FaultAction(className = DeniedAccessException_Exception.class, value = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/openSession/Fault/DeniedAccessException")})
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", className = "com.rencredit.jschool.boruak.taskmanager.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", className = "com.rencredit.jschool.boruak.taskmanager.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.rencredit.jschool.boruak.taskmanager.endpoint.Session openSession(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    ) throws EmptyUserException_Exception, EmptyPasswordException_Exception, EmptyHashLineException_Exception, EmptyLoginException_Exception, DeniedAccessException_Exception;

    @WebMethod
    @Action(input = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/closeSessionRequest", output = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/closeSessionResponse", fault = {@FaultAction(className = DeniedAccessException_Exception.class, value = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/SessionEndpoint/closeSession/Fault/DeniedAccessException")})
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", className = "com.rencredit.jschool.boruak.taskmanager.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/", className = "com.rencredit.jschool.boruak.taskmanager.endpoint.CloseSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.rencredit.jschool.boruak.taskmanager.endpoint.Result closeSession(
        @WebParam(name = "session", targetNamespace = "")
        com.rencredit.jschool.boruak.taskmanager.endpoint.Session session
    ) throws DeniedAccessException_Exception;
}
