package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISystemObjectRepository {

    @Nullable
    Session putSession(@NotNull final Session session);

    @Nullable
    Session getSession();

    @Nullable
    Session removeSession();

    void clearAll();

}
