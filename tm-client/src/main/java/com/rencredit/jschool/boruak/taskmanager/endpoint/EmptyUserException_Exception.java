
package com.rencredit.jschool.boruak.taskmanager.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-01-17T19:09:43.141+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "EmptyUserException", targetNamespace = "http://endpoint.taskmanager.boruak.jschool.rencredit.com/")
public class EmptyUserException_Exception extends Exception {

    private com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyUserException emptyUserException;

    public EmptyUserException_Exception() {
        super();
    }

    public EmptyUserException_Exception(String message) {
        super(message);
    }

    public EmptyUserException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public EmptyUserException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyUserException emptyUserException) {
        super(message);
        this.emptyUserException = emptyUserException;
    }

    public EmptyUserException_Exception(String message, com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyUserException emptyUserException, java.lang.Throwable cause) {
        super(message, cause);
        this.emptyUserException = emptyUserException;
    }

    public com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyUserException getFaultInfo() {
        return this.emptyUserException;
    }
}
