package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class TaskRepositoryTest {

    @Test
    public void testAdd() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.getList().isEmpty());
        @NotNull final Task task1 = new Task("1", "name", "description");
        taskRepository.add(task1);
        @NotNull final Task taskFromRepository = taskRepository.getList().get(0);
        Assert.assertNotNull(taskFromRepository);
        Assert.assertNotNull(taskFromRepository.getId());
        Assert.assertNotNull(taskFromRepository.getName());
    }

    @Test
    public void testRemove() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        final int sizeList = taskRepository.getList().size();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        taskRepository.add(task1);

        Assert.assertTrue(taskRepository.remove(task1));
        Assert.assertFalse(taskRepository.remove(task1));
        Assert.assertEquals(sizeList, taskRepository.getList().size());
    }

    @Test
    public void testGetList() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        Assert.assertFalse(taskRepository.getList().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        Assert.assertFalse(taskRepository.getList().isEmpty());
        taskRepository.clearAll();
        Assert.assertTrue(taskRepository.getList().isEmpty());
    }

    @Test
    public void testLoadCollection() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Collection<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        tasks.add(task1);
        @NotNull final Task task2 = new Task("1", "name2", "description");
        tasks.add(task2);
        @NotNull final Task task3 = new Task("1", "name3", "description");
        tasks.add(task3);

        Assert.assertTrue(taskRepository.getList().isEmpty());
        taskRepository.load(tasks);
        Assert.assertEquals(3, taskRepository.getList().size());
    }

    @Test
    public void testLoadVararg() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        @NotNull final Task task2 = new Task("1", "name2", "description");
        @NotNull final Task task3 = new Task("1", "name3", "description");

        Assert.assertTrue(taskRepository.getList().isEmpty());
        taskRepository.load(task1, task2, task3);
        Assert.assertEquals(3, taskRepository.getList().size());
    }

    @Test
    public void testMergeOne() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        taskRepository.merge(task1);

        Assert.assertEquals(1, taskRepository.getList().size());
    }

    @Test
    public void testMergeCollection() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Collection<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        tasks.add(task1);
        @NotNull final Task task2 = new Task("1", "name2", "description");
        tasks.add(task2);
        @NotNull final Task task3 = new Task("1", "name3", "description");
        tasks.add(task3);

        Assert.assertTrue(taskRepository.getList().isEmpty());
        taskRepository.merge(tasks);
        Assert.assertEquals(3, taskRepository.getList().size());
    }

    @Test
    public void testMergeVararg() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        @NotNull final Task task2 = new Task("1", "name2", "description");
        @NotNull final Task task3 = new Task("1", "name3", "description");

        Assert.assertTrue(taskRepository.getList().isEmpty());
        taskRepository.merge(task1, task2, task3);
        Assert.assertEquals(3, taskRepository.getList().size());
    }

    @Test
    public void testFindAll() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        @NotNull final List<Task> taskList = taskRepository.getList();
        Assert.assertEquals(taskList.size(), 5);
    }

    @Test
    public void testClearByUserId() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        taskRepository.clearByUserId("1");
        taskRepository.clearByUserId("7");
        @NotNull final List<Task> taskList = taskRepository.getList();
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void testFindAllByUserId() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        @NotNull final List<Task> taskList = taskRepository.findAllByUserId("1");
        Assert.assertEquals(3, taskList.size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final Task task = new Task("1", "name", "description");
        taskRepository.add(task);

        @NotNull final String id = task.getId();
        @Nullable final Task taskFind = taskRepository.findOneById("1", id);
        Assert.assertEquals(task, taskFind);
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final Task task = new Task("1", "name", "description");
        taskRepository.add(task);

        @Nullable final Task taskFind = taskRepository.findOneByIndex("1", 3);
        Assert.assertEquals(task, taskFind);
    }

    @Test
    public void testFindOneByName() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final Task task = new Task("1", "name777", "description");
        taskRepository.add(task);

        @Nullable final Task taskFind = taskRepository.findOneByName("1", "name777");
        Assert.assertEquals(task, taskFind);
    }

    @Test
    public void testFindNotExist() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        @Nullable final Task taskFindById = taskRepository.findOneById("1", "000");
        Assert.assertNull(taskFindById);
        @Nullable final Task taskFindByIndex = taskRepository.findOneByIndex("1", 10);
        Assert.assertNull(taskFindByIndex);
        @Nullable final Task taskFindByName = taskRepository.findOneByName("1", "000");
        Assert.assertNull(taskFindByName);
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final int sizeList = taskRepository.getList().size();
        @NotNull final Task task = new Task("1", "name", "description");
        taskRepository.add(task);

        @NotNull final String id = task.getId();
        @Nullable final Task taskRemoved = taskRepository.removeOneById("1", id);
        Assert.assertEquals(task, taskRemoved);
        Assert.assertEquals(sizeList, taskRepository.getList().size());
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final int sizeList = taskRepository.getList().size();
        @NotNull final Task task = new Task("1", "name", "description");
        taskRepository.add(task);

        @Nullable final Task taskRemoved = taskRepository.removeOneByIndex("1", 3);
        Assert.assertEquals(task, taskRemoved);
        Assert.assertEquals(sizeList, taskRepository.getList().size());
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final int sizeList = taskRepository.getList().size();
        @NotNull final Task task = new Task("1", "name777", "description");
        taskRepository.add(task);

        @Nullable final Task taskRemoved = taskRepository.removeOneByName("1", "name777");
        Assert.assertEquals(task, taskRemoved);
        Assert.assertEquals(sizeList, taskRepository.getList().size());
    }

    @Test
    public void testRemoveNotExist() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        @Nullable final Task taskRemovedById = taskRepository.removeOneById("1", "000");
        Assert.assertNull(taskRemovedById);
        @Nullable final Task taskRemovedByIndex = taskRepository.removeOneByIndex("1", 10);
        Assert.assertNull(taskRemovedByIndex);
        @Nullable final Task taskRemovedByName = taskRepository.removeOneByName("1", "000");
        Assert.assertNull(taskRemovedByName);
    }

    public TaskRepository getFullTaskRepository() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        taskRepository.add(task1);
        @NotNull final Task task2 = new Task("1", "name2", "description");
        taskRepository.add(task2);
        @NotNull final Task task3 = new Task("1", "name3", "description");
        taskRepository.add(task3);
        @NotNull final Task task4 = new Task("2", "name4", "description");
        taskRepository.add(task4);
        @NotNull final Task task5 = new Task("7", "name5", "description");
        taskRepository.add(task5);
        return taskRepository;
    }

}
