package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class TaskServiceTest {

    @NotNull TaskRepository taskRepository;
    @NotNull TaskService taskService;

    @Before
    public void init() {
        taskRepository = new TaskRepository();
        taskService = new TaskService(taskRepository);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithoutUserId() throws EmptyNameException, EmptyUserIdException {
        taskService.create(null, "Demo");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithoutName() throws EmptyNameException, EmptyUserIdException {
        taskService.create("1", (String) null);
    }

    @Test
    public void testCreateUserIdName() throws EmptyNameException, EmptyUserIdException {
        final boolean result = taskService.create("1", "name");
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutUserId() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        taskService.create(null, "Demo", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutName() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        taskService.create("1", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutDescription() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        taskService.create("1", "Demo", null);
    }

    @Test
    public void testCreateUserIdNameDescription() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        final boolean result = taskService.create("1", "name", "description");
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdTaskWithoutUserId() throws EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create(null, task);
    }

    @Test(expected = EmptyTaskException.class)
    public void testNegativeCreateUserIdTaskWithoutTask() throws EmptyUserIdException, EmptyTaskException {
        taskService.create("1", (Task) null);
    }

    @Test
    public void testCreateUserIdTask() throws EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        final boolean result = taskService.create("1", task);
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllByUserIdWithoutUserId() throws EmptyUserIdException {
        taskService.findAllByUserId(null);

    }

    @Test
    public void testFindAllByUserId() throws EmptyUserIdException, EmptyTaskException {
        @NotNull final List<Task> emptyListTask = taskService.findAllByUserId("1");
        Assert.assertTrue(emptyListTask.isEmpty());
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @NotNull final List<Task> notEmptyListTask = taskService.findAllByUserId("1");
        Assert.assertFalse(notEmptyListTask.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllByUserIdWithoutUserId() throws EmptyUserIdException {
        taskService.clearByUserId(null);

    }

    @Test
    public void testClearAllByUserId() throws EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @NotNull final List<Task> notEmptyListTask = taskService.findAllByUserId("1");
        Assert.assertFalse(notEmptyListTask.isEmpty());
        taskService.clearByUserId("1");
        @NotNull final List<Task> emptyListTask = taskService.findAllByUserId("1");
        Assert.assertTrue(emptyListTask.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByIndexWithoutUserId() throws EmptyUserIdException, IncorrectIndexException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task findTask = taskService.findOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneByIndexWithoutIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task findTask = taskService.findOneByIndex("1", null);
    }

    @Test
    public void testFindOneByIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task findTask = taskService.findOneByIndex("1", 0);
        Assert.assertEquals(task, findTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameWithoutUserId() throws EmptyUserIdException, EmptyNameException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task findTask = taskService.findOneByName(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameWithoutName() throws EmptyUserIdException, EmptyNameException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task findTask = taskService.findOneByName("1", null);
    }

    @Test
    public void testFindOneByName() throws EmptyUserIdException, EmptyNameException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task findTask = taskService.findOneByName("1", "name");
        Assert.assertEquals(task, findTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByIdWithoutUserId() throws EmptyIdException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.findOneById(null, task.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneByIdWithoutId() throws EmptyIdException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.findOneById("1", null);
    }

    @Test
    public void testFindOneById() throws EmptyIdException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task findTask = taskService.findOneById("1", task.getId());
        Assert.assertEquals(task, findTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIdWithoutUserId() throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskById(null, "1", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateTaskByIdWithoutId() throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskById("1", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIdWithoutName() throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskById("1", "1", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIdWithoutDescription() throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskById("1", "1", "name", null);
    }

    @Test
    public void testUpdateTaskById() throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @NotNull final Task updatedTask = taskService.updateTaskById("1", task.getId(), "name2", "description2");
        Assert.assertEquals("name2", updatedTask.getName());
        Assert.assertEquals("description2", updatedTask.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateTaskByIndexWithoutUserId() throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateTaskByIndexWithoutIndex() throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskByIndex("1", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateTaskByIndexWithoutName() throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskByIndex("1", 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateTaskByIndexWithoutDescription() throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.updateTaskByIndex("1", 0, "name", null);
    }

    @Test
    public void testUpdateTaskByIndex() throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, EmptyDescriptionException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @NotNull final Task updatedTask = taskService.updateTaskByIndex("1", 0, "name2", "description2");
        Assert.assertEquals("name2", updatedTask.getName());
        Assert.assertEquals("description2", updatedTask.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithoutUserId() throws EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        final boolean result = taskService.remove(null, task);
    }

    @Test(expected = EmptyTaskException.class)
    public void testNegativeRemoveWithoutTask() throws EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        final boolean result = taskService.remove("1", null);
    }

    @Test
    public void testRemove() throws EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        final boolean result = taskService.remove("1", task);
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithoutUserId() throws EmptyUserIdException, IncorrectIndexException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.removeOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithoutIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.removeOneByIndex("1", null);
    }

    @Test
    public void testRemoveOneByIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task removedTask = taskService.removeOneByIndex("1", 0);
        Assert.assertEquals(task, removedTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByNameWithoutUserId() throws EmptyNameException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.removeOneByName(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneByNameWithoutName() throws EmptyNameException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.removeOneByName("1", null);
    }

    @Test
    public void testRemoveOneByName() throws EmptyNameException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task removedTask = taskService.removeOneByName("1", "name");
        Assert.assertEquals(task, removedTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithoutUserId() throws EmptyIdException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.removeOneById(null, task.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithoutId() throws EmptyIdException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        taskService.removeOneById("1", null);
    }

    @Test
    public void testRemoveOneById() throws EmptyIdException, EmptyUserIdException, EmptyTaskException {
        @NotNull final Task task = new Task("1", "name");
        taskService.create("1", task);
        @Nullable final Task removedTask = taskService.removeOneById("1", task.getId());
        Assert.assertEquals(task, removedTask);
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadCollectionWithoutCollection() throws EmptyElementsException {
        taskService.load((Collection<Task>) null);
    }

    @Test
    public void testLoadCollection() throws EmptyElementsException {
        @NotNull final Collection<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        tasks.add(task1);
        @NotNull final Task task2 = new Task("1", "name2", "description");
        tasks.add(task2);
        @NotNull final Task task3 = new Task("1", "name3", "description");
        tasks.add(task3);

        Assert.assertTrue(taskService.getList().isEmpty());
        taskService.load(tasks);
        Assert.assertEquals(3, taskService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadVarargWithoutVararg() throws EmptyElementsException {
        taskService.load();
    }

    @Test
    public void testLoadVararg() throws EmptyElementsException {
        @NotNull final Task task1 = new Task("1", "name1", "description");
        @NotNull final Task task2 = new Task("1", "name2", "description");
        @NotNull final Task task3 = new Task("1", "name3", "description");

        Assert.assertTrue(taskService.getList().isEmpty());
        taskService.load(task1, task2, task3);
        Assert.assertEquals(3, taskService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeMergeOneWithoutElement() throws EmptyElementsException {
        taskService.merge((Task) null);
    }

    @Test
    public void testMergeOne() throws EmptyElementsException {
        @NotNull final Task task1 = new Task("1", "name1", "description");
        taskService.merge(task1);

        Assert.assertEquals(1, taskService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeCollectionWithoutCollection() throws NotExistAbstractListException {
        taskService.merge((Collection<Task>) null);
    }

    @Test
    public void testMergeCollection() throws NotExistAbstractListException {
        @NotNull final Collection<Task> tasks = new ArrayList<>();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        tasks.add(task1);
        @NotNull final Task task2 = new Task("1", "name2", "description");
        tasks.add(task2);
        @NotNull final Task task3 = new Task("1", "name3", "description");
        tasks.add(task3);

        Assert.assertTrue(taskService.getList().isEmpty());
        taskService.merge(tasks);
        Assert.assertEquals(3, taskService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeVarargWithoutVararg() throws NotExistAbstractListException {
        taskService.merge();
    }

    @Test
    public void testMergeVararg() throws NotExistAbstractListException {
        @NotNull final Task task1 = new Task("1", "name1", "description");
        @NotNull final Task task2 = new Task("1", "name2", "description");
        @NotNull final Task task3 = new Task("1", "name3", "description");

        Assert.assertTrue(taskService.getList().isEmpty());
        taskService.merge(task1, task2, task3);
        Assert.assertEquals(3, taskService.getList().size());
    }

    @Test
    public void testGetList() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final TaskService taskService = new TaskService(taskRepository);

        Assert.assertFalse(taskService.getList().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final TaskService taskService = new TaskService(taskRepository);

        Assert.assertFalse(taskService.getList().isEmpty());
        taskService.clearAll();
        Assert.assertTrue(taskService.getList().isEmpty());
    }

    private TaskRepository getFullTaskRepository() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Task task1 = new Task("1", "name1", "description");
        taskRepository.add(task1);
        @NotNull final Task task2 = new Task("1", "name2", "description");
        taskRepository.add(task2);
        @NotNull final Task task3 = new Task("1", "name3", "description");
        taskRepository.add(task3);
        @NotNull final Task task4 = new Task("2", "name4", "description");
        taskRepository.add(task4);
        @NotNull final Task task5 = new Task("7", "name5", "description");
        taskRepository.add(task5);
        return taskRepository;
    }

}
