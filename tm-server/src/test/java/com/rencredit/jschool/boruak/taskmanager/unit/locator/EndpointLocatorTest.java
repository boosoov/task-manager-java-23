package com.rencredit.jschool.boruak.taskmanager.unit.locator;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class EndpointLocatorTest {

    @Test
    public void endpointLocatorTest() {
        @NotNull final IServiceLocator serviceLocator = new ServiceLocator();
        @NotNull final EndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

        Assert.assertNotNull(endpointLocator.getServiceLocator());
        Assert.assertNotNull(endpointLocator.getUserEndpoint());
        Assert.assertNotNull(endpointLocator.getSessionEndpoint());
        Assert.assertNotNull(endpointLocator.getTaskEndpoint());

        Assert.assertNotNull(endpointLocator.getProjectEndpoint());
        Assert.assertNotNull(endpointLocator.getAdminEndpoint());
        Assert.assertNotNull(endpointLocator.getAdminUserEndpoint());
        Assert.assertNotNull(endpointLocator.getAuthEndpoint());
    }

}
