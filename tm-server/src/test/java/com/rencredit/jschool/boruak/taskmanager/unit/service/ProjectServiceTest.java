package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class ProjectServiceTest {

    @NotNull ProjectRepository projectRepository;
    @NotNull ProjectService projectService;

    @Before
    public void init() {
        projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameWithoutUserId() throws EmptyNameException, EmptyUserIdException {
        projectService.create(null, "Demo");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameWithoutName() throws EmptyNameException, EmptyUserIdException {
        projectService.create("1", (String) null);
    }

    @Test
    public void testCreateUserIdName() throws EmptyNameException, EmptyUserIdException {
        final boolean result = projectService.create("1", "name");
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutUserId() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        projectService.create(null, "Demo", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutName() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        projectService.create("1", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeCreateUserIdNameDescriptionWithoutDescription() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        projectService.create("1", "Demo", null);
    }

    @Test
    public void testCreateUserIdNameDescription() throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException {
        final boolean result = projectService.create("1", "name", "description");
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeCreateUserIdProjectWithoutUserId() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create(null, project);
    }

    @Test(expected = EmptyProjectException.class)
    public void testNegativeCreateUserIdProjectWithoutProject() throws EmptyProjectException, EmptyUserIdException {
        projectService.create("1", (Project) null);
    }

    @Test
    public void testCreateUserIdProject() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final Project project = new Project("1", "name");
        final boolean result = projectService.create("1", project);
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindAllByUserIdWithoutUserId() throws EmptyUserIdException {
        projectService.findAllByUserId(null);

    }

    @Test
    public void testFindAllByUserId() throws EmptyUserIdException, EmptyProjectException {
        @NotNull final List<Project> emptyListProject = projectService.findAllByUserId("1");
        Assert.assertTrue(emptyListProject.isEmpty());
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @NotNull final List<Project> notEmptyListProject = projectService.findAllByUserId("1");
        Assert.assertFalse(notEmptyListProject.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeClearAllByUserIdWithoutUserId() throws EmptyUserIdException {
        projectService.clearByUserId(null);

    }

    @Test
    public void testClearAllByUserId() throws EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @NotNull final List<Project> notEmptyListProject = projectService.findAllByUserId("1");
        Assert.assertFalse(notEmptyListProject.isEmpty());
        projectService.clearByUserId("1");
        @NotNull final List<Project> emptyListProject = projectService.findAllByUserId("1");
        Assert.assertTrue(emptyListProject.isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByIndexWithoutUserId() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project findProject = projectService.findOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeFindOneByIndexWithoutIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project findProject = projectService.findOneByIndex("1", null);
    }

    @Test
    public void testFindOneByIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project findProject = projectService.findOneByIndex("1", 0);
        Assert.assertEquals(project, findProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByNameWithoutUserId() throws EmptyProjectException, EmptyUserIdException, EmptyNameException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project findProject = projectService.findOneByName(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeFindOneByNameWithoutName() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project findProject = projectService.findOneByName("1", null);
    }

    @Test
    public void testFindOneByName() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project findProject = projectService.findOneByName("1", "name");
        Assert.assertEquals(project, findProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeFindOneByIdWithoutUserId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.findOneById(null, project.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeFindOneByIdWithoutId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.findOneById("1", null);
    }

    @Test
    public void testFindOneById() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project findProject = projectService.findOneById("1", project.getId());
        Assert.assertEquals(project, findProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIdWithoutUserId() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectById(null, "1", "name", "description");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateProjectByIdWithoutId() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectById("1", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIdWithoutName() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectById("1", "1", null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIdWithoutDescription() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectById("1", "1", "name", null);
    }

    @Test
    public void testUpdateProjectById() throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @NotNull final Project updatedProject = projectService.updateProjectById("1", project.getId(), "name2", "description2");
        Assert.assertEquals("name2", updatedProject.getName());
        Assert.assertEquals("description2", updatedProject.getDescription());
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeUpdateProjectByIndexWithoutUserId() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectByIndex(null, 0, "name", "description");
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeUpdateProjectByIndexWithoutIndex() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectByIndex("1", null, "name", "description");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeUpdateProjectByIndexWithoutName() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectByIndex("1", 0, null, "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void testNegativeUpdateProjectByIndexWithoutDescription() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, EmptyDescriptionException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.updateProjectByIndex("1", 0, "name", null);
    }

    @SneakyThrows
    @Test
    public void testUpdateProjectByIndex() throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @NotNull final Project updatedProject = projectService.updateProjectByIndex("1", 0, "name2", "description2");
        Assert.assertEquals("name2", updatedProject.getName());
        Assert.assertEquals("description2", updatedProject.getDescription());
    }

    @SneakyThrows
    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveWithoutUserId() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        final boolean result = projectService.remove(null, project);
    }

    @SneakyThrows
    @Test(expected = EmptyProjectException.class)
    public void testNegativeRemoveWithoutProject() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        final boolean result = projectService.remove("1", null);
    }

    @SneakyThrows
    @Test
    public void testRemove() throws EmptyProjectException, EmptyUserIdException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        final boolean result = projectService.remove("1", project);
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIndexWithoutUserId() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.removeOneByIndex(null, 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void testNegativeRemoveOneByIndexWithoutIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.removeOneByIndex("1", null);
    }

    @Test
    public void testRemoveOneByIndex() throws EmptyUserIdException, IncorrectIndexException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project removedProject = projectService.removeOneByIndex("1", 0);
        Assert.assertEquals(project, removedProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByNameWithoutUserId() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.removeOneByName(null, "name");
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativeRemoveOneByNameWithoutName() throws EmptyNameException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.removeOneByName("1", null);
    }

    @Test
    public void testRemoveOneByName() throws EmptyProjectException, EmptyUserIdException, EmptyNameException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project removedProject = projectService.removeOneByName("1", "name");
        Assert.assertEquals(project, removedProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void testNegativeRemoveOneByIdWithoutUserId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.removeOneById(null, project.getId());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveOneByIdWithoutId() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        projectService.removeOneById("1", null);
    }

    @Test
    public void testRemoveOneById() throws EmptyIdException, EmptyUserIdException, EmptyProjectException {
        @NotNull final Project project = new Project("1", "name");
        projectService.create("1", project);
        @Nullable final Project removedProject = projectService.removeOneById("1", project.getId());
        Assert.assertEquals(project, removedProject);
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadCollectionWithoutCollection() throws EmptyElementsException {
        projectService.load((Collection<Project>) null);
    }

    @Test
    public void testLoadCollection() throws EmptyElementsException {
        @NotNull final Collection<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projects.add(project1);
        @NotNull final Project project2 = new Project("1", "name2", "description");
        projects.add(project2);
        @NotNull final Project project3 = new Project("1", "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.load(projects);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadVarargWithoutVararg() throws EmptyElementsException {
        projectService.load();
    }

    @Test
    public void testLoadVararg() throws EmptyElementsException {
        @NotNull final Project project1 = new Project("1", "name1", "description");
        @NotNull final Project project2 = new Project("1", "name2", "description");
        @NotNull final Project project3 = new Project("1", "name3", "description");

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.load(project1, project2, project3);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeMergeOneWithoutElement() throws EmptyElementsException {
        projectService.merge((Project) null);
    }

    @Test
    public void testMergeOne() throws EmptyElementsException {
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projectService.merge(project1);

        Assert.assertEquals(1, projectService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeCollectionWithoutCollection() throws NotExistAbstractListException {
        projectService.merge((Collection<Project>) null);
    }

    @Test
    public void testMergeCollection() throws NotExistAbstractListException {
        @NotNull final Collection<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projects.add(project1);
        @NotNull final Project project2 = new Project("1", "name2", "description");
        projects.add(project2);
        @NotNull final Project project3 = new Project("1", "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.merge(projects);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeVarargWithoutVararg() throws NotExistAbstractListException {
        projectService.merge();
    }

    @Test
    public void testMergeVararg() throws NotExistAbstractListException {
        @NotNull final Project project1 = new Project("1", "name1", "description");
        @NotNull final Project project2 = new Project("1", "name2", "description");
        @NotNull final Project project3 = new Project("1", "name3", "description");

        Assert.assertTrue(projectService.getList().isEmpty());
        projectService.merge(project1, project2, project3);
        Assert.assertEquals(3, projectService.getList().size());
    }

    @Test
    public void testGetList() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final ProjectService projectService = new ProjectService(projectRepository);

        Assert.assertFalse(projectService.getList().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final ProjectService projectService = new ProjectService(projectRepository);

        Assert.assertFalse(projectService.getList().isEmpty());
        projectService.clearAll();
        Assert.assertTrue(projectService.getList().isEmpty());
    }

    private ProjectRepository getFullProjectRepository() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projectRepository.add(project1);
        @NotNull final Project project2 = new Project("1", "name2", "description");
        projectRepository.add(project2);
        @NotNull final Project project3 = new Project("1", "name3", "description");
        projectRepository.add(project3);
        @NotNull final Project project4 = new Project("2", "name4", "description");
        projectRepository.add(project4);
        @NotNull final Project project5 = new Project("7", "name5", "description");
        projectRepository.add(project5);
        return projectRepository;
    }

}
