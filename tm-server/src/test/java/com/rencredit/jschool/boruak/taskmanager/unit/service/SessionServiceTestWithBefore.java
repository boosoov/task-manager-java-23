package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.SessionRepository;
import com.rencredit.jschool.boruak.taskmanager.service.SessionService;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import com.rencredit.jschool.boruak.taskmanager.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class SessionServiceTestWithBefore {

    @NotNull IServiceLocator serviceLocator;
    @NotNull ISessionService sessionService;
    @NotNull IPropertyService propertyService;

    @Nullable String passwordHash;
    @NotNull User user;
    @NotNull Session session;


    @Before
    public void init() throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException, BusyLoginException {
        serviceLocator = new ServiceLocator();
        sessionService = serviceLocator.getSessionService();
        propertyService = serviceLocator.getPropertyService();

        propertyService.init();
        passwordHash = HashUtil.getHashLine("password");
        user = new User("login", passwordHash);
        serviceLocator.getUserService().add("login", user);
        session = sessionService.open("login", "password");
    }

    @Test
    public void testOpen() {
        Assert.assertNotNull(session.getSignature());
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getTimestamp());
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeOpenWithoutLogin() throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException {
        sessionService.open(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeOpenWithoutPassword() throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException {
        sessionService.open("login", null);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeOpenWithoutUserInBase() throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException {
        sessionService.open("login2", "password2");
    }

    @Test
    public void testClose() throws DeniedAccessException {
        Assert.assertTrue(sessionService.isValid(session));
        sessionService.close(session);
        Assert.assertFalse(sessionService.isValid(session));
    }

    @Test
    public void testCloseAll() throws DeniedAccessException {
        Assert.assertTrue(sessionService.isValid(session));
        sessionService.closeAll(session);
        Assert.assertFalse(sessionService.isValid(session));
    }

    @Test
    public void testGetUser() throws EmptyIdException, DeniedAccessException {
        Assert.assertEquals(user, sessionService.getUser(session));
    }

    @Test
    public void testGetUserId() throws DeniedAccessException {
        Assert.assertEquals(session.getUserId(), sessionService.getUserId(session));
    }

    @Test
    public void testGetListSession() throws DeniedAccessException {
        Assert.assertEquals(session.getUserId(), sessionService.getUserId(session));
    }

    @Test
    public void testSign() {
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        @NotNull final Session sessionReceived = sessionService.sign(session);
        Assert.assertEquals(signature, sessionReceived.getSignature());
    }

    @Test
    public void testIsValid() {
        Assert.assertTrue(sessionService.isValid(session));
        @NotNull final Session incorrectSession = new Session();
        Assert.assertFalse(sessionService.isValid(incorrectSession));
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionWithoutSession() throws DeniedAccessException {
        sessionService.validate(null);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionWithoutSignature() throws DeniedAccessException {
        session.setSignature(null);
        sessionService.validate(session);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionWithoutUserId() throws DeniedAccessException {
        session.setUserId(null);
        sessionService.validate(session);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionWithoutTimeStamp() throws DeniedAccessException {
        session.setTimestamp(null);
        sessionService.validate(session);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionWithSpoiledSignature() throws DeniedAccessException {
        session.setSignature("spoiledSignature");
        sessionService.validate(session);
    }

    @Test
    public void testValidateSession() throws DeniedAccessException {
        sessionService.validate(session);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionRoleWithoutSession() throws EmptyIdException, DeniedAccessException {
        sessionService.validate(null, Role.USER);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionRoleWithoutRole() throws EmptyIdException, DeniedAccessException {
        sessionService.validate(session, null);
    }

    @Test(expected = DeniedAccessException.class)
    public void testNegativeValidateSessionRoleWithoutUserId() throws EmptyIdException, DeniedAccessException {
        sessionService.validate(session, Role.ADMIN);
    }

    @Test
    public void testValidateSessionRole() throws EmptyIdException, DeniedAccessException {
        sessionService.validate(session, Role.USER);
    }

    @Test
    public void testNegativeCheckUserAccessWithoutUserInBase() throws EmptyLoginException, EmptyHashLineException {
        final boolean result = sessionService.checkUserAccess("login2", "password2");
        Assert.assertFalse(result);
    }

    @Test
    public void testNegativeCheckUserAccessWithoutPasswordHash() throws EmptyLoginException, EmptyHashLineException {
        final boolean result = sessionService.checkUserAccess("login2", "password2");
        Assert.assertFalse(result);
    }

    @Test
    public void testNegativeCheckUserAccessWithSpoiledPasswordHash() throws EmptyLoginException, EmptyHashLineException {
        user.setPasswordHash("SpoiledPasswordHash");
        final boolean result = sessionService.checkUserAccess("login", "password");
        Assert.assertFalse(result);
    }

    @Test
    public void testCheckUserAccess() throws EmptyLoginException, EmptyHashLineException {
        final boolean result = sessionService.checkUserAccess("login", "password");
        Assert.assertTrue(result);
    }

}
