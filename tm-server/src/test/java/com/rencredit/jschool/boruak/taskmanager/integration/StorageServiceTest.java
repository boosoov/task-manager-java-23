package com.rencredit.jschool.boruak.taskmanager.integration;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IStorageService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationFileSystemTestCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;

@Category(IntegrationFileSystemTestCategory.class)
public class StorageServiceTest {

    @NotNull IServiceLocator serviceLocator;
    @NotNull IStorageService storageService;
    @NotNull IUserService userService;
    @NotNull IProjectService projectService;
    @NotNull ITaskService taskService;

    @Before
    public void init() {
        serviceLocator = new ServiceLocator();
        storageService = serviceLocator.getStorageService();
        userService = serviceLocator.getUserService();
        taskService = serviceLocator.getTaskService();
        projectService = serviceLocator.getProjectService();
    }

    @SneakyThrows
    @Test
    public void testSaveLoadClearBase64() {
        userService.add("login", "password");
        taskService.create("userId", "name");
        projectService.create("userId", "name");

        storageService.saveBase64();

        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        Assert.assertTrue(file.exists());

        userService.clearAll();
        taskService.clearAll();
        projectService.clearAll();

        Assert.assertTrue(userService.getList().isEmpty());
        Assert.assertTrue(taskService.getList().isEmpty());
        Assert.assertTrue(projectService.getList().isEmpty());

        storageService.loadBase64();

        Assert.assertFalse(userService.getList().isEmpty());
        Assert.assertFalse(taskService.getList().isEmpty());
        Assert.assertFalse(projectService.getList().isEmpty());

        storageService.clearBase64();
        Assert.assertFalse(file.exists());
    }

    @SneakyThrows
    @Test
    public void testSaveLoadClearBinary() {
        userService.add("login", "password");
        taskService.create("userId", "name");
        projectService.create("userId", "name");

        storageService.saveBinary();

        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        Assert.assertTrue(file.exists());

        userService.clearAll();
        taskService.clearAll();
        projectService.clearAll();

        Assert.assertTrue(userService.getList().isEmpty());
        Assert.assertTrue(taskService.getList().isEmpty());
        Assert.assertTrue(projectService.getList().isEmpty());

        storageService.loadBinary();

        Assert.assertFalse(userService.getList().isEmpty());
        Assert.assertFalse(taskService.getList().isEmpty());
        Assert.assertFalse(projectService.getList().isEmpty());

        storageService.clearBinary();
        Assert.assertFalse(file.exists());
    }

    @SneakyThrows
    @Test
    public void testSaveLoadClearJson() {
        userService.add("login", "password");
        taskService.create("userId", "name");
        projectService.create("userId", "name");

        storageService.saveJson();

        @NotNull final File file = new File(DataConstant.FILE_JSON);
        Assert.assertTrue(file.exists());

        userService.clearAll();
        taskService.clearAll();
        projectService.clearAll();

        Assert.assertTrue(userService.getList().isEmpty());
        Assert.assertTrue(taskService.getList().isEmpty());
        Assert.assertTrue(projectService.getList().isEmpty());

        storageService.loadJson();

        Assert.assertFalse(userService.getList().isEmpty());
        Assert.assertFalse(taskService.getList().isEmpty());
        Assert.assertFalse(projectService.getList().isEmpty());

        storageService.clearJson();
        Assert.assertFalse(file.exists());
    }

    @SneakyThrows
    @Test
    public void testSaveLoadClearXml() {
        userService.add("login", "password");
        taskService.create("userId", "name");
        projectService.create("userId", "name");

        storageService.saveXml();

        @NotNull final File file = new File(DataConstant.FILE_XML);
        Assert.assertTrue(file.exists());

        userService.clearAll();
        taskService.clearAll();
        projectService.clearAll();

        Assert.assertTrue(userService.getList().isEmpty());
        Assert.assertTrue(taskService.getList().isEmpty());
        Assert.assertTrue(projectService.getList().isEmpty());

        storageService.loadXml();

        Assert.assertFalse(userService.getList().isEmpty());
        Assert.assertFalse(taskService.getList().isEmpty());
        Assert.assertFalse(projectService.getList().isEmpty());

        storageService.clearXml();
        Assert.assertFalse(file.exists());
    }

}
