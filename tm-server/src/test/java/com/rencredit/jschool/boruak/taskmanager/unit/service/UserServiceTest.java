package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;

@Category(UnitTestCategory.class)
public class UserServiceTest {

    @NotNull UserRepository userRepository;
    @NotNull UserService userService;

    @Before
    public void init() {
        userRepository = new UserRepository();
        userService = new UserService(userRepository);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeAddLoginPasswordWithoutLogin() throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeAddLoginPasswordWithoutPassword() throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add("login", (String) null);
    }

    @Test
    public void testAddLoginPassword() throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        final boolean result = userService.add("login", "password");
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeAddLoginPasswordFirstNameWithoutLogin() throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add(null, "password", "firstName");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeAddLoginPasswordFirstNameWithoutPassword() throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add("login", null, "firstName");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeAddLoginPasswordFirstNameWithoutFirstName() throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add("login", "password", (String) null);
    }

    @Test
    public void testAddLoginPasswordFirstName() throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        final boolean result = userService.add("login", "password", "firstName");
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeAddLoginPasswordRoleWithoutLogin() throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add(null, "password", Role.USER);
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeAddLoginPasswordRoleWithoutPassword() throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add("login", null, Role.USER);
    }

    @Test(expected = EmptyRoleException.class)
    public void testNegativeAddLoginPasswordRoleWithoutRole() throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add("login", "password", (Role) null);
    }

    @Test
    public void testAddLoginPasswordRole() throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        final boolean result = userService.add("login", "password", Role.USER);
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeAddLoginUserWithoutLogin() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        userService.add(null, new User("login", "password"));
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeAddLoginUserWithoutUser() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        userService.add("login", (User) null);
    }

    @Test
    public void testAddLoginUser() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        final boolean result = userService.add("login", new User("login", "password"));
        Assert.assertTrue(result);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeGetByIdWithoutId() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        userService.add("login", new User("login", "password"));
        userService.getById(null);
    }

    @Test
    public void testGetById() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userFromService = userService.getById(user.getId());
        Assert.assertEquals(user, userFromService);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeGetByLoginWithoutLogin() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        userService.add("login", new User("login", "password"));
        userService.getByLogin(null);
    }

    @Test
    public void testGetByLogin() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userFromService = userService.getByLogin(user.getLogin());
        Assert.assertEquals(user, userFromService);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdatePasswordByIdWithoutId() throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException {
        userService.updatePasswordById(null, "password");
    }

    @Test(expected = EmptyNewPasswordException.class)
    public void testNegativeUpdatePasswordByIdWithoutNewPassword() throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException {
        userService.updatePasswordById("login", null);
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeUpdatePasswordByIdWithoutUserInBase() throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException {
        userService.updatePasswordById("login", "password");
    }

    @Test
    public void testUpdatePasswordById() throws EmptyHashLineException, EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyLoginException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @NotNull final User userUpdated = userService.updatePasswordById(user.getId(), "password");
        @Nullable final String passwordHash = HashUtil.getHashLine("password");
        Assert.assertEquals(passwordHash, userUpdated.getPasswordHash());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeEditProfileByIdFirstNameWithoutId() throws EmptyIdException, EmptyUserException, EmptyFirstNameException {
        userService.editProfileById(null, "firstName");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeEditProfileByIdFirstNameWithoutFirstName() throws EmptyIdException, EmptyUserException, EmptyFirstNameException {
        userService.editProfileById("id", null);
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeEditProfileByIdFirstNameWithoutUserInBase() throws EmptyIdException, EmptyUserException, EmptyFirstNameException {
        userService.editProfileById("id", "firstName");
    }

    @Test
    public void testEditProfileByIdFirstName() throws EmptyIdException, EmptyUserException, EmptyFirstNameException, EmptyLoginException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userUpdated = userService.editProfileById(user.getId(), "firstName");
        Assert.assertEquals("firstName", userUpdated.getFirstName());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeEditProfileByIdFirstNameLastNameWithoutId() throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException {
        userService.editProfileById(null, "firstName", "lastName");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeEditProfileByIdFirstNameLastNameWithoutFirstName() throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException {
        userService.editProfileById("id", null, "lastName");
    }

    @Test(expected = EmptyLastNameException.class)
    public void testNegativeEditProfileByIdFirstNameLastNameWithoutLastName() throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException {
        userService.editProfileById("id", "firstName", null);
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeEditProfileByIdFirstNameLastNameWithoutUserInBase() throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException {
        userService.editProfileById("id", "firstName", "lastName");
    }

    @Test
    public void testEditProfileByIdFirstNameLastName() throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException, EmptyLoginException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userUpdated = userService.editProfileById(user.getId(), "firstName", "lastName");
        Assert.assertEquals("firstName", userUpdated.getFirstName());
        Assert.assertEquals("lastName", userUpdated.getLastName());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeEditProfileByIdEmailFirstNameLastNameMiddleNameWithoutId() throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException {
        userService.editProfileById(null,"email", "firstName", "lastName", "middleName");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeEditProfileByIdEmailFirstNameLastNameMiddleNameWithoutEmail() throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException {
        userService.editProfileById("id",null, "firstName", "lastName", "middleName");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeEditProfileByIdEmailFirstNameLastNameMiddleNameWithoutFirstName() throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException {
        userService.editProfileById("id","email", null, "lastName", "middleName");
    }

    @Test(expected = EmptyLastNameException.class)
    public void testNegativeEditProfileByIdEmailFirstNameLastNameMiddleNameWithoutLastName() throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException {
        userService.editProfileById("id","email", "firstName", null, "middleName");
    }

    @Test(expected = EmptyMiddleNameException.class)
    public void testNegativeEditProfileByIdEmailFirstNameLastNameMiddleNameWithoutMiddleName() throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException {
        userService.editProfileById("id","email", "firstName", "lastName", null);
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeEditProfileByIdEmailFirstNameLastNameMiddleNameWithoutUserInBase() throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException {
        userService.editProfileById("id","email", "firstName", "lastName", "middleName");
    }

    @Test
    public void testEditProfileByIdEmailFirstNameLastNameMiddleName() throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException, EmptyLoginException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userUpdated = userService.editProfileById(user.getId(),"email", "firstName", "lastName", "middleName");
        Assert.assertEquals("email", userUpdated.getEmail());
        Assert.assertEquals("firstName", userUpdated.getFirstName());
        Assert.assertEquals("lastName", userUpdated.getLastName());
        Assert.assertEquals("middleName", userUpdated.getMiddleName());
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeRemoveByIdWithoutId() throws EmptyIdException {
        userService.removeById(null);
    }

    @Test
    public void testNegativeRemoveByIdWithoutUserInBase() throws EmptyIdException {
        Assert.assertNull(userService.removeById("id"));
    }

    @Test
    public void testRemoveById() throws EmptyIdException, EmptyLoginException, EmptyUserException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userRemoved = userService.removeById(user.getId());
        Assert.assertEquals(user, userRemoved);
        Assert.assertTrue(userService.getList().isEmpty());
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRemoveByLoginWithoutLogin() throws EmptyLoginException {
        userService.removeByLogin(null);
    }

    @Test
    public void testNegativeRemoveByLoginWithoutUserInBase() throws EmptyLoginException {
        Assert.assertNull(userService.removeByLogin("Login"));
    }

    @Test
    public void testRemoveByLogin() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userRemoved = userService.removeByLogin(user.getLogin());
        Assert.assertEquals(user, userRemoved);
        Assert.assertTrue(userService.getList().isEmpty());
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeRemoveByUserWithoutUser() throws EmptyUserException {
        userService.removeByUser(null);
    }

    @Test
    public void testNegativeRemoveByUserWithoutUserInBase() throws EmptyUserException {
        @NotNull final User user = new User("login", "password");
        Assert.assertNull(userService.removeByUser(user));
    }

    @Test
    public void testRemoveByUser() throws EmptyUserException, EmptyLoginException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userRemoved = userService.removeByUser(user);
        Assert.assertEquals(user, userRemoved);
        Assert.assertTrue(userService.getList().isEmpty());
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeLockUserByLoginWithoutUser() throws EmptyLoginException, EmptyUserException {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeLockUserByLoginWithoutUserInBase() throws EmptyLoginException, EmptyUserException {
        userService.lockUserByLogin("login");
    }

    @Test
    public void testLockUserByLogin() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userLocked = userService.lockUserByLogin("login");
        Assert.assertTrue(userLocked.isLocked());
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUnlockUserByLoginWithoutUser() throws EmptyLoginException, EmptyUserException {
        userService.unlockUserByLogin(null);
    }

    @Test(expected = EmptyUserException.class)
    public void testNegativeUnlockUserByLoginWithoutUserInBase() throws EmptyLoginException, EmptyUserException {
        userService.unlockUserByLogin("login");
    }

    @Test
    public void testUnlockUserByLogin() throws EmptyLoginException, EmptyUserException, BusyLoginException {
        @NotNull final User user = new User("login", "password");
        userService.add("login", user);
        @Nullable final User userLocked = userService.unlockUserByLogin("login");
        Assert.assertFalse(userLocked.isLocked());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadCollectionWithoutCollection() throws EmptyElementsException {
        userService.load((Collection<User>) null);
    }

    @Test
    public void testLoadCollection() throws EmptyElementsException {
        @NotNull final Collection<User> users = new ArrayList<>();
        @NotNull final User user1 = new User("1", "name1", "description");
        users.add(user1);
        @NotNull final User user2 = new User("1", "name2", "description");
        users.add(user2);
        @NotNull final User user3 = new User("1", "name3", "description");
        users.add(user3);

        Assert.assertTrue(userService.getList().isEmpty());
        userService.load(users);
        Assert.assertEquals(3, userService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeLoadVarargWithoutVararg() throws EmptyElementsException {
        userService.load();
    }

    @Test
    public void testLoadVararg() throws EmptyElementsException {
        @NotNull final User user1 = new User("1", "name1", "description");
        @NotNull final User user2 = new User("1", "name2", "description");
        @NotNull final User user3 = new User("1", "name3", "description");

        Assert.assertTrue(userService.getList().isEmpty());
        userService.load(user1, user2, user3);
        Assert.assertEquals(3, userService.getList().size());
    }

    @Test(expected = EmptyElementsException.class)
    public void testNegativeMergeOneWithoutElement() throws EmptyElementsException {

        userService.merge((User) null);
    }

    @Test
    public void testMergeOne() throws EmptyElementsException {
        @NotNull final User user1 = new User("1", "name1", "description");
        userService.merge(user1);

        Assert.assertEquals(1, userService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeCollectionWithoutCollection() throws NotExistAbstractListException {
        userService.merge((Collection<User>) null);
    }

    @Test
    public void testMergeCollection() throws NotExistAbstractListException {
        @NotNull final Collection<User> users = new ArrayList<>();
        @NotNull final User user1 = new User("1", "name1", "description");
        users.add(user1);
        @NotNull final User user2 = new User("1", "name2", "description");
        users.add(user2);
        @NotNull final User user3 = new User("1", "name3", "description");
        users.add(user3);

        Assert.assertTrue(userService.getList().isEmpty());
        userService.merge(users);
        Assert.assertEquals(3, userService.getList().size());
    }

    @Test(expected = NotExistAbstractListException.class)
    public void testNegativeMergeVarargWithoutVararg() throws NotExistAbstractListException {
        userService.merge();
    }

    @Test
    public void testMergeVararg() throws NotExistAbstractListException {
        @NotNull final User user1 = new User("1", "name1", "description");
        @NotNull final User user2 = new User("1", "name2", "description");
        @NotNull final User user3 = new User("1", "name3", "description");

        Assert.assertTrue(userService.getList().isEmpty());
        userService.merge(user1, user2, user3);
        Assert.assertEquals(3, userService.getList().size());
    }

    @Test
    public void testGetList() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final UserService userService = new UserService(userRepository);

        Assert.assertFalse(userService.getList().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final UserService userService = new UserService(userRepository);

        Assert.assertFalse(userService.getList().isEmpty());
        userService.clearAll();
        Assert.assertTrue(userService.getList().isEmpty());
    }

    private UserRepository getFullUserRepository() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final User user1 = new User("1", "name1", "description");
        userRepository.add(user1);
        @NotNull final User user2 = new User("1", "name2", "description");
        userRepository.add(user2);
        @NotNull final User user3 = new User("1", "name3", "description");
        userRepository.add(user3);
        @NotNull final User user4 = new User("2", "name4", "description");
        userRepository.add(user4);
        @NotNull final User user5 = new User("7", "name5", "description");
        userRepository.add(user5);
        return userRepository;
    }

}
