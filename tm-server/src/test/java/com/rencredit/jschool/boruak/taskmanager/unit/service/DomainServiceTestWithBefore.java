package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IDomainService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import com.rencredit.jschool.boruak.taskmanager.service.DomainService;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;

@Category(UnitTestCategory.class)
public class DomainServiceTestWithBefore {

    @NotNull private IProjectService projectService;
    @NotNull private ITaskService taskService;
    @NotNull private IUserService userService;
    @NotNull private IDomainService domainService;

    @Before
    public void init() {
        projectService = new ProjectService(new ProjectRepository());
        taskService = new TaskService(new TaskRepository());
        userService = new UserService(new UserRepository());
        domainService = new DomainService(projectService, taskService, userService);
    }

    @Test
    public void testExport() throws EmptyNameException, EmptyUserIdException, EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        userService.add("login", "password");
        taskService.create("userId", "name");
        projectService.create("userId", "name");
        @NotNull final Domain domain = new Domain();

        Assert.assertTrue(domain.getUsers().isEmpty());
        Assert.assertTrue(domain.getTasks().isEmpty());
        Assert.assertTrue(domain.getProjects().isEmpty());
        domainService.export(domain);
        Assert.assertFalse(domain.getUsers().isEmpty());
        Assert.assertFalse(domain.getTasks().isEmpty());
        Assert.assertFalse(domain.getProjects().isEmpty());
    }

    @Test
    public void testLoad() throws EmptyElementsException {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(Arrays.asList(new Project("userId", "name")));
        domain.setTasks(Arrays.asList(new Task("userId", "name")));
        domain.setUsers(Arrays.asList(new User("login", "password")));

        Assert.assertTrue(userService.getList().isEmpty());
        Assert.assertTrue(taskService.getList().isEmpty());
        Assert.assertTrue(projectService.getList().isEmpty());
        domainService.load(domain);
        Assert.assertFalse(userService.getList().isEmpty());
        Assert.assertFalse(taskService.getList().isEmpty());
        Assert.assertFalse(projectService.getList().isEmpty());
    }

}
