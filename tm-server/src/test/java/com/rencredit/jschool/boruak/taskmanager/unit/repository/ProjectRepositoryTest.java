package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class ProjectRepositoryTest {

    @Test
    public void testAdd() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.getList().isEmpty());
        @NotNull final Project project1 = new Project("1", "name", "description");
        projectRepository.add(project1);
        @NotNull final Project projectFromRepository = projectRepository.getList().get(0);
        Assert.assertNotNull(projectFromRepository);
        Assert.assertNotNull(projectFromRepository.getId());
        Assert.assertNotNull(projectFromRepository.getName());
    }

    @Test
    public void testRemove() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        final int sizeList = projectRepository.getList().size();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projectRepository.add(project1);

        Assert.assertTrue(projectRepository.remove(project1));
        Assert.assertFalse(projectRepository.remove(project1));
        Assert.assertEquals(sizeList, projectRepository.getList().size());
    }

    @Test
    public void testGetList() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        Assert.assertFalse(projectRepository.getList().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        Assert.assertFalse(projectRepository.getList().isEmpty());
        projectRepository.clearAll();
        Assert.assertTrue(projectRepository.getList().isEmpty());
    }

    @Test
    public void testLoadCollection() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Collection<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projects.add(project1);
        @NotNull final Project project2 = new Project("1", "name2", "description");
        projects.add(project2);
        @NotNull final Project project3 = new Project("1", "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectRepository.getList().isEmpty());
        projectRepository.load(projects);
        Assert.assertEquals(3, projectRepository.getList().size());
    }

    @Test
    public void testLoadVararg() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        @NotNull final Project project2 = new Project("1", "name2", "description");
        @NotNull final Project project3 = new Project("1", "name3", "description");

        Assert.assertTrue(projectRepository.getList().isEmpty());
        projectRepository.load(project1, project2, project3);
        Assert.assertEquals(3, projectRepository.getList().size());
    }

    @Test
    public void testMergeOne() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projectRepository.merge(project1);

        Assert.assertEquals(1, projectRepository.getList().size());
    }

    @Test
    public void testMergeCollection() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Collection<Project> projects = new ArrayList<>();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projects.add(project1);
        @NotNull final Project project2 = new Project("1", "name2", "description");
        projects.add(project2);
        @NotNull final Project project3 = new Project("1", "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectRepository.getList().isEmpty());
        projectRepository.merge(projects);
        Assert.assertEquals(3, projectRepository.getList().size());
    }

    @Test
    public void testMergeVararg() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        @NotNull final Project project2 = new Project("1", "name2", "description");
        @NotNull final Project project3 = new Project("1", "name3", "description");

        Assert.assertTrue(projectRepository.getList().isEmpty());
        projectRepository.merge(project1, project2, project3);
        Assert.assertEquals(3, projectRepository.getList().size());
    }

    @Test
    public void testFindAll() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        @NotNull final List<Project> projectList = projectRepository.getList();
        Assert.assertEquals(projectList.size(), 5);
    }

    @Test
    public void testClearByUserId() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        projectRepository.clearByUserId("1");
        projectRepository.clearByUserId("7");
        @NotNull final List<Project> projectList = projectRepository.getList();
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void testFindAllByUserId() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        @NotNull final List<Project> projectList = projectRepository.findAllByUserId("1");
        Assert.assertEquals(3, projectList.size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final Project project = new Project("1", "name", "description");
        projectRepository.add(project);

        @NotNull final String id = project.getId();
        @Nullable final Project projectFind = projectRepository.findOneById("1", id);
        Assert.assertEquals(project, projectFind);
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final Project project = new Project("1", "name", "description");
        projectRepository.add(project);

        @Nullable final Project projectFind = projectRepository.findOneByIndex("1", 3);
        Assert.assertEquals(project, projectFind);
    }

    @Test
    public void testFindOneByName() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final Project project = new Project("1", "name777", "description");
        projectRepository.add(project);

        @Nullable final Project projectFind = projectRepository.findOneByName("1", "name777");
        Assert.assertEquals(project, projectFind);
    }

    @Test
    public void testFindNotExist() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        @Nullable final Project projectFindById = projectRepository.findOneById("1", "000");
        Assert.assertNull(projectFindById);
        @Nullable final Project projectFindByIndex = projectRepository.findOneByIndex("1", 10);
        Assert.assertNull(projectFindByIndex);
        @Nullable final Project projectFindByName = projectRepository.findOneByName("1", "000");
        Assert.assertNull(projectFindByName);
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final int sizeList = projectRepository.getList().size();
        @NotNull final Project project = new Project("1", "name", "description");
        projectRepository.add(project);

        @NotNull final String id = project.getId();
        @Nullable final Project projectRemoved = projectRepository.removeOneById("1", id);
        Assert.assertEquals(project, projectRemoved);
        Assert.assertEquals(sizeList, projectRepository.getList().size());
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final int sizeList = projectRepository.getList().size();
        @NotNull final Project project = new Project("1", "name", "description");
        projectRepository.add(project);

        @Nullable final Project projectRemoved = projectRepository.removeOneByIndex("1", 3);
        Assert.assertEquals(project, projectRemoved);
        Assert.assertEquals(sizeList, projectRepository.getList().size());
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final int sizeList = projectRepository.getList().size();
        @NotNull final Project project = new Project("1", "name777", "description");
        projectRepository.add(project);

        @Nullable final Project projectRemoved = projectRepository.removeOneByName("1", "name777");
        Assert.assertEquals(project, projectRemoved);
        Assert.assertEquals(sizeList, projectRepository.getList().size());
    }

    @Test
    public void testRemoveNotExist() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        @Nullable final Project projectRemovedById = projectRepository.removeOneById("1", "000");
        Assert.assertNull(projectRemovedById);
        @Nullable final Project projectRemovedByIndex = projectRepository.removeOneByIndex("1", 10);
        Assert.assertNull(projectRemovedByIndex);
        @Nullable final Project projectRemovedByName = projectRepository.removeOneByName("1", "000");
        Assert.assertNull(projectRemovedByName);
    }

    private ProjectRepository getFullProjectRepository() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Project project1 = new Project("1", "name1", "description");
        projectRepository.add(project1);
        @NotNull final Project project2 = new Project("1", "name2", "description");
        projectRepository.add(project2);
        @NotNull final Project project3 = new Project("1", "name3", "description");
        projectRepository.add(project3);
        @NotNull final Project project4 = new Project("2", "name4", "description");
        projectRepository.add(project4);
        @NotNull final Project project5 = new Project("7", "name5", "description");
        projectRepository.add(project5);
        return projectRepository;
    }

}
