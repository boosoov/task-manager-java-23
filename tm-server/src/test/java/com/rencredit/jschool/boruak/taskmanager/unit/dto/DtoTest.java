package com.rencredit.jschool.boruak.taskmanager.unit.dto;

import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.dto.Fail;
import com.rencredit.jschool.boruak.taskmanager.dto.Result;
import com.rencredit.jschool.boruak.taskmanager.dto.Success;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;

@Category(UnitTestCategory.class)
public class DtoTest {

    @Test
    public void resultTest() {
        @NotNull final Result result = new Result();
        Assert.assertSame(true, result.success);
        Assert.assertSame("", result.message);
    }

    @Test
    public void successTest() {
        @NotNull final Success success = new Success();
        Assert.assertSame(true, success.success);
        Assert.assertSame("", success.message);
    }

    @Test
    public void failTest() {
        @NotNull final Fail fail = new Fail();
        Assert.assertSame(false, fail.success);
        Assert.assertSame("", fail.message);

        @NotNull final Fail failWithException = new Fail(new EmptyNameException());
        Assert.assertSame(false, failWithException.success);
        Assert.assertSame("Error! Name is empty...", failWithException.message);
    }

    @Test
    public void domainTest() {
        @NotNull final Domain domain = new Domain();
        Assert.assertTrue(domain.getUsers().isEmpty());
        Assert.assertTrue(domain.getProjects().isEmpty());
        Assert.assertTrue(domain.getTasks().isEmpty());

        domain.setProjects(Arrays.asList(new Project("userId", "name")));
        domain.setTasks(Arrays.asList(new Task("userId", "name")));
        domain.setUsers(Arrays.asList(new User("login", "password")));

        Assert.assertFalse(domain.getUsers().isEmpty());
        Assert.assertFalse(domain.getProjects().isEmpty());
        Assert.assertFalse(domain.getTasks().isEmpty());
    }

}
