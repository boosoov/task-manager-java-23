package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class UserRepositoryTest {

    @Test
    public void testAdd() {
        @NotNull final UserRepository userRepository = new UserRepository();
        Assert.assertTrue(userRepository.getList().isEmpty());
        @NotNull final User user1 = new User("login", "passwordHash");
        userRepository.add(user1);
        @NotNull final User userFromRepository = userRepository.getList().get(0);
        Assert.assertNotNull(userFromRepository);
    }

    @Test
    public void testRemove() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        final int sizeList = userRepository.getList().size();
        @NotNull final User user1 = new User("login", "passwordHash");
        userRepository.add(user1);

        Assert.assertTrue(userRepository.remove(user1));
        Assert.assertFalse(userRepository.remove(user1));
        Assert.assertEquals(sizeList, userRepository.getList().size());
    }

    @Test
    public void testGetList() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        Assert.assertFalse(userRepository.getList().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        Assert.assertFalse(userRepository.getList().isEmpty());
        userRepository.clearAll();
        Assert.assertTrue(userRepository.getList().isEmpty());
    }

    @Test
    public void testLoadCollection() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final Collection<User> users = new ArrayList<>();
        @NotNull final User user1 = new User("login1", "passwordHash1");
        users.add(user1);
        @NotNull final User user2 = new User("login2", "passwordHash2");
        users.add(user2);
        @NotNull final User user3 = new User("login3", "passwordHash3");
        users.add(user3);

        Assert.assertTrue(userRepository.getList().isEmpty());
        userRepository.load(users);
        Assert.assertEquals(3, userRepository.getList().size());
    }

    @Test
    public void testLoadVararg() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final User user1 = new User("login1", "passwordHash1");
        @NotNull final User user2 = new User("login2", "passwordHash2");
        @NotNull final User user3 = new User("login3", "passwordHash3");

        Assert.assertTrue(userRepository.getList().isEmpty());
        userRepository.load(user1, user2, user3);
        Assert.assertEquals(3, userRepository.getList().size());
    }

    @Test
    public void testMergeOne() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final User user1 = new User("login", "passwordHash");
        userRepository.merge(user1);

        Assert.assertEquals(1, userRepository.getList().size());
    }

    @Test
    public void testMergeCollection() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final Collection<User> users = new ArrayList<>();
        @NotNull final User user1 = new User("login1", "passwordHash1");
        users.add(user1);
        @NotNull final User user2 = new User("login2", "passwordHash2");
        users.add(user2);
        @NotNull final User user3 = new User("login3", "passwordHash3");
        users.add(user3);

        Assert.assertTrue(userRepository.getList().isEmpty());
        userRepository.merge(users);
        Assert.assertEquals(3, userRepository.getList().size());
    }

    @Test
    public void testMergeVararg() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final User user1 = new User("login1", "passwordHash1");
        @NotNull final User user2 = new User("login2", "passwordHash2");
        @NotNull final User user3 = new User("login3", "passwordHash3");

        Assert.assertTrue(userRepository.getList().isEmpty());
        userRepository.merge(user1, user2, user3);
        Assert.assertEquals(3, userRepository.getList().size());
    }

    @Test
    public void testFindAll() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        @NotNull final List<User> userList = userRepository.getList();
        Assert.assertEquals(userList.size(), 5);
    }

    @Test
    public void testFindById() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final User user = new User("login", "passwordHash");
        userRepository.add(user);

        @NotNull final String id = user.getId();
        @Nullable final User userFind = userRepository.findById(id);
        Assert.assertEquals(user, userFind);
    }

    @Test
    public void testFindByLogin() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final User user = new User("login", "passwordHash");
        userRepository.add(user);

        @Nullable final User userFind = userRepository.findByLogin("login");
        Assert.assertEquals(user, userFind);
    }

    @Test
    public void testFindNotExist() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        @Nullable final User userFindById = userRepository.findById("000");
        Assert.assertNull(userFindById);
        @Nullable final User userFindByIndex = userRepository.findByLogin("000");
        Assert.assertNull(userFindByIndex);
    }

    @Test
    public void testRemoveById() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final int sizeList = userRepository.getList().size();
        @NotNull final User user = new User("login", "passwordHash");
        userRepository.add(user);

        @NotNull final String id = user.getId();
        @Nullable final User userRemoved = userRepository.removeById(id);
        Assert.assertEquals(user, userRemoved);
        Assert.assertEquals(sizeList, userRepository.getList().size());
    }

    @Test
    public void testRemoveByLogin() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final int sizeList = userRepository.getList().size();
        @NotNull final User user = new User("login", "passwordHash");
        userRepository.add(user);

        @Nullable final User userRemoved = userRepository.removeByLogin("login");
        Assert.assertEquals(user, userRemoved);
        Assert.assertEquals(sizeList, userRepository.getList().size());
    }

    @Test
    public void testRemoveByUser() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final int sizeList = userRepository.getList().size();
        @NotNull final User user = new User("login", "passwordHash");
        userRepository.add(user);

        @Nullable final User userRemoved = userRepository.removeByUser(user);
        Assert.assertEquals(user, userRemoved);
        Assert.assertEquals(sizeList, userRepository.getList().size());
    }

    @Test
    public void testRemoveNotExist() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        @Nullable final User userRemovedById = userRepository.removeById("000");
        Assert.assertNull(userRemovedById);
        @Nullable final User userRemovedByIndex = userRepository.removeByLogin("000");
        Assert.assertNull(userRemovedByIndex);
        @NotNull final User user = new User("000", "passwordHash");
        @Nullable final User userRemovedByName = userRepository.removeByUser(user);
        Assert.assertNull(userRemovedByName);
    }

    public UserRepository getFullUserRepository() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final User user1 = new User("login1", "passwordHash1");
        userRepository.add(user1);
        @NotNull final User user2 = new User("login2", "passwordHash2");
        userRepository.add(user2);
        @NotNull final User user3 = new User("login3", "passwordHash3");
        userRepository.add(user3);
        @NotNull final User user4 = new User("login4", "passwordHash4");
        userRepository.add(user4);
        @NotNull final User user5 = new User("login5", "passwordHash5");
        userRepository.add(user5);
        return userRepository;
    }

}
