package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.ITaskEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.ITaskService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    private ITaskService taskService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        taskService = serviceLocator.getTaskService();
    }

    @Override
    @WebMethod
    public void createTaskName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createTaskNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTaskByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findAllByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyUserIdException, EmptyNameException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.findOneByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, DeniedAccessException, EmptyDescriptionException {
        sessionService.validate(session);
        return taskService.updateTaskById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, DeniedAccessException, EmptyDescriptionException {
        sessionService.validate(session);
        return taskService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return taskService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeAllUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        taskService.clearByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearAllTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        taskService.clearAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getListTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        return taskService.getList();
    }

}
