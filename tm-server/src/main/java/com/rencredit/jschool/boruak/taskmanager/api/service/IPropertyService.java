package com.rencredit.jschool.boruak.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    boolean init();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionSalt();

    @NotNull
    Integer getSessionCycle();

}
