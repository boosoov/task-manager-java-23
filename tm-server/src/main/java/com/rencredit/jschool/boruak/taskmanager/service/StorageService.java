package com.rencredit.jschool.boruak.taskmanager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.constant.DataConstant;
import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyElementsException;
import org.jetbrains.annotations.NotNull;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class StorageService implements IStorageService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public StorageService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;

    }

    @Override
    public boolean saveBase64() throws IOException {
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);

        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.FILE_BASE64);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        return true;
    }

    @Override
    public boolean loadBase64() throws IOException, ClassNotFoundException, EmptyElementsException {
        @NotNull final String base64Data = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_BASE64)));
        @NotNull final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
        return true;
    }

    @Override
    public boolean clearBase64() throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        return true;
    }

    @Override
    public boolean saveBinary() throws IOException {
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
        return true;
    }

    @Override
    public boolean loadBinary() throws IOException, ClassNotFoundException, EmptyElementsException {
        @NotNull final ObjectInputStream objectInputStream;
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
        objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.load(domain.getProjects());
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.load(domain.getTasks());
        @NotNull final IUserService userService = serviceLocator.getUserService();
        userService.load(domain.getUsers());
        objectInputStream.close();
        fileInputStream.close();
        return true;
    }

    @Override
    public boolean clearBinary() throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        return true;
    }

    @Override
    public boolean saveJson() throws IOException {
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);

        @NotNull final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.FILE_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        return true;
    }

    @Override
    public boolean loadJson() throws IOException, EmptyElementsException {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_JSON)));
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        return true;
    }

    @Override
    public boolean clearJson() throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        return true;
    }

    @Override
    public boolean saveXml() throws IOException {
        @NotNull final Domain domain = new Domain();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.export(domain);

        @NotNull final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.FILE_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        return true;
    }

    @Override
    public boolean loadXml() throws IOException, EmptyElementsException {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_XML)));
        @NotNull final Domain domain = xmlMapper.readValue(xml, Domain.class);
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        return true;
    }

    @Override
    public boolean clearXml() throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_XML);
        Files.deleteIfExists(file.toPath());
        return true;
    }

}
