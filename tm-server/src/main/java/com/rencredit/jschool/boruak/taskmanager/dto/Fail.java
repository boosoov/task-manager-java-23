package com.rencredit.jschool.boruak.taskmanager.dto;

import org.jetbrains.annotations.Nullable;

public class Fail extends Result {

    public Fail() {
        success = false;
        message = "";
    }

    public Fail(@Nullable final Exception e) {
        success = false;
        if (e == null) return;
        message = e.getMessage();
    }

}

