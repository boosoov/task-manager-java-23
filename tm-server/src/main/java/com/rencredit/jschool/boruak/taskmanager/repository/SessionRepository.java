package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ISessionRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean removeByUserId(@NotNull final String Id) {
        @Nullable final Session session = findById(Id);
        if (session == null) return false;
        return removeBySession(session);
    }

    @Override
    public boolean removeBySession(@NotNull final Session session) {
        return remove(session);
    }

    @Nullable
    @Override
    public Session findById(@NotNull final String Id) {
        for (@NotNull final Session session : getList()) {
            if (Id.equals(session.getId())) return session;
        }
        return null;
    }

    @Override
    public boolean contains(@NotNull final String Id) {
        @Nullable final Session session = findById(Id);
        return getList().contains(session);
    }

}
