package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.IProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final Project project) {
        return remove(project);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> userProject = new ArrayList<>();
        for (@NotNull final Project project : getList()) {
            if (userId.equals(project.getUserId())) {
                userProject.add(project);
            }
        }
        return userProject;
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        @NotNull List<Project> projects = getList();
        for (int i = 0; i < projects.size(); i++) {
            if (userId.equals(projects.get(i).getUserId())) {
                projects.remove(projects.get(i));
                i--;
            }
        }
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Project project : getList()) {
            if (userId.equals(project.getUserId()) && id.equals(project.getId())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Project> userProjects = new ArrayList<>();
        for (@NotNull Project project : getList()) {
            if (userId.equals(project.getUserId())) userProjects.add(project);
        }
        if (userProjects.size() < index) return null;
        return userProjects.get(index);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Project project : getList()) {
            if (userId.equals(project.getUserId()) &&
                    name.equals(project.getName())) return project;
        }
        return null;
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(userId, project);
        return project;
    }

}
