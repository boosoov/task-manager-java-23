package com.rencredit.jschool.boruak.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Setter
@Getter
public class Task extends AbstractEntity implements Serializable {

    public static final long serialVersionUID = 1;

    @Nullable
    private String name;

    @Nullable
    private String description = "";

    @Nullable
    private String userId;

    public Task() {
    }

    public Task(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        this.userId = userId;
        this.name = name;
    }

    public Task(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + super.getId() + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }

}
