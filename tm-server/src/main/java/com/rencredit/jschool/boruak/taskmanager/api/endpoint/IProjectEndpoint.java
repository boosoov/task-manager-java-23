package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void createProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void createProjectNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException, DeniedAccessException;

    @NotNull
    @WebMethod
    List<Project> findAllProjectByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    Project findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    Project findProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException;

    @Nullable
    @WebMethod
    Project findProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @NotNull
    @WebMethod
    Project updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, DeniedAccessException, EmptyDescriptionException;

    @NotNull
    @WebMethod
    Project updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, DeniedAccessException, EmptyDescriptionException;

    @Nullable
    @WebMethod
    Project removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    Project removeProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException;

    @Nullable
    @WebMethod
    Project removeProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void clearAllUserProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void clearAllProject(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException;

    @NotNull
    @WebMethod
    List<Project> getListProject(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException;

}
