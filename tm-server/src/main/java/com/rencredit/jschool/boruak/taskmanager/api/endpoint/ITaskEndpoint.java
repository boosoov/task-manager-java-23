package com.rencredit.jschool.boruak.taskmanager.api.endpoint;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createTaskName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void createTaskNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException, DeniedAccessException;

    @NotNull
    @WebMethod
    List<Task> findAllTaskByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    Task findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    Task findTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException;

    @Nullable
    @WebMethod
    Task findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyUserIdException, EmptyNameException, DeniedAccessException;

    @NotNull
    @WebMethod
    Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, DeniedAccessException, EmptyDescriptionException;

    @NotNull
    @WebMethod
    Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws IncorrectIndexException, EmptyTaskException, EmptyNameException, EmptyUserIdException, DeniedAccessException, EmptyDescriptionException;

    @Nullable
    @WebMethod
    Task removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException;

    @Nullable
    @WebMethod
    Task removeTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "index", partName = "index") final Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException;

    @Nullable
    @WebMethod
    Task removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void removeAllUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws EmptyUserIdException, DeniedAccessException;

    @WebMethod
    void clearAllTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException;

    @NotNull
    @WebMethod
    List<Task> getListTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException;

}
