package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import org.jetbrains.annotations.NotNull;

public interface IAuthRepository {

    void logIn(@NotNull User user);

    void logOut();

}
