package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<Project> {

    boolean create(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    boolean create(@Nullable String userId, @Nullable String name, @Nullable String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    boolean create(@Nullable final String userId, @Nullable Project project) throws EmptyProjectException, EmptyUserIdException;

    boolean remove(@Nullable String userId, @Nullable Project project) throws EmptyProjectException, EmptyUserIdException;

    @NotNull
    List<Project> findAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    void clearByUserId(@Nullable String userId) throws EmptyUserIdException;

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    Project findOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    Project removeOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @NotNull
    Project updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws EmptyProjectException, EmptyNameException, EmptyIdException, EmptyUserIdException, EmptyDescriptionException;

    @NotNull
    Project updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws EmptyProjectException, EmptyNameException, IncorrectIndexException, EmptyUserIdException, EmptyDescriptionException;

}
