package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    boolean remove(@NotNull String userId, @NotNull Project project);

    void clearByUserId(@NotNull String userId);

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeOneByName(@NotNull String userId, @NotNull String name);

}
