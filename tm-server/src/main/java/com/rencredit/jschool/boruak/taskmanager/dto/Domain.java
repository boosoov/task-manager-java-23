package com.rencredit.jschool.boruak.taskmanager.dto;

import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
public final class Domain implements Serializable {

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @NotNull
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    private List<User> users = new ArrayList<>();

    public void setProjects(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

    public void setTasks(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    public void setUsers(@Nullable final List<User> users) {
        this.users = users;
    }
}
