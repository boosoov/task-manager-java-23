package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.AbstractEntity;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyElementsException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    void load(@Nullable Collection<E> elements) throws EmptyElementsException;

    void load(@Nullable E... elements) throws EmptyElementsException;

    boolean merge(@Nullable E element) throws EmptyElementsException;

    void merge(@Nullable Collection<E> elements) throws NotExistAbstractListException;

    void merge(@Nullable E... elements) throws NotExistAbstractListException;

    void clearAll();

    @NotNull
    List<E> getList();

}
