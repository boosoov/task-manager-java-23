package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionRepository extends IRepository<Session> {

    boolean removeByUserId(@NotNull String Id);

    boolean removeBySession(@NotNull final Session session);

    @Nullable
    Session findById(@NotNull String id);

    boolean contains(@NotNull String id);

}
