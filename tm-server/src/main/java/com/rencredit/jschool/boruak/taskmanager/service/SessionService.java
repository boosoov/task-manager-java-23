package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ISessionRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.util.HashUtil;
import com.rencredit.jschool.boruak.taskmanager.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository repository;

    public SessionService(@NotNull final IServiceLocator serviceLocator, @NotNull final ISessionRepository repository) {
        this.serviceLocator = serviceLocator;
        this.repository = repository;
    }

    @NotNull
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) throws EmptyPasswordException, EmptyLoginException, EmptyHashLineException, DeniedAccessException, EmptyUserException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        final boolean check = checkUserAccess(login, password);
        if (!check) throw new DeniedAccessException();
        final User user = serviceLocator.getUserService().getByLogin(login);
        if (user == null) throw new EmptyUserException();
        if (user.isLocked()) throw new DeniedAccessException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        repository.merge(session);
        return sign(session);
    }

    @Override
    public boolean close(@Nullable final Session session) throws DeniedAccessException {
        validate(session);
        return repository.remove(session);
    }

    @Override
    public void closeAll(@Nullable final Session session) throws DeniedAccessException {
        validate(session);
        repository.clearAll();
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) throws DeniedAccessException, EmptyIdException {
        @Nullable final String userId = getUserId(session);
        return serviceLocator.getUserService().getById(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable final Session session) throws DeniedAccessException {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getListSession(@Nullable final Session session) throws DeniedAccessException {
        validate(session);
        return repository.getList();
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final Session session) throws DeniedAccessException {
        if (session == null) throw new DeniedAccessException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new DeniedAccessException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new DeniedAccessException();
        if (session.getTimestamp() == null) throw new DeniedAccessException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new DeniedAccessException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new DeniedAccessException();
        if (!repository.contains(session.getId())) throw new DeniedAccessException();
    }


    @Override
    public void validate(@Nullable final Session session, @Nullable final Role role) throws DeniedAccessException, EmptyIdException {
        if (role == null) throw new DeniedAccessException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().getById(userId);
        if (user == null) throw new DeniedAccessException();
        if (!role.equals((user.getRole()))) throw new DeniedAccessException();
    }

    @Override
    public boolean checkUserAccess(@NotNull final String login, @NotNull final String password) throws EmptyHashLineException, EmptyLoginException {
        @Nullable final User user = serviceLocator.getUserService().getByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.getHashLine(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

}
