package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IAdminEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IStorageService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;

@WebService
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    private IStorageService storageService;

    public AdminEndpoint() {
    }

    public AdminEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
        storageService = serviceLocator.getStorageService();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @WebMethod
    public boolean clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.clearBase64();
        return true;
    }

    @Override
    @WebMethod
    public boolean loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, ClassNotFoundException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadBase64();
        return true;
    }

    @Override
    @WebMethod
    public boolean saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveBase64();
        return true;
    }

    @Override
    @WebMethod
    public boolean clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.clearBinary();
        return true;
    }

    @Override
    @WebMethod
    public boolean loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, ClassNotFoundException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadBinary();
        return true;
    }

    @Override
    @WebMethod
    public boolean saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveBinary();
        return true;
    }

    @Override
    @WebMethod
    public boolean cleanJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.clearJson();
        return true;
    }

    @Override
    @WebMethod
    public boolean loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadJson();
        return true;
    }

    @Override
    @WebMethod
    public boolean saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveJson();
        return true;
    }

    @Override
    @WebMethod
    public boolean clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.clearXml();
        return true;
    }

    @Override
    @WebMethod
    public boolean loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyElementsException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.loadXml();
        return true;
    }

    @Override
    @WebMethod
    public boolean saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        storageService.saveXml();
        return true;
    }

    @Override
    @WebMethod
    public void shutDownServer(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws IOException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles());
        System.exit(0);
    }

}
