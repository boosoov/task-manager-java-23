package com.rencredit.jschool.boruak.taskmanager.dto;

import org.jetbrains.annotations.NotNull;

public class Result {

    @NotNull
    public Boolean success = true;

    @NotNull
    public String message = "";

}
