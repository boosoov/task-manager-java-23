package com.rencredit.jschool.boruak.taskmanager.bootstrap;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IEndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.locator.EndpointLocator;
import com.rencredit.jschool.boruak.taskmanager.locator.ServiceLocator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.ws.Endpoint;

public class Bootstrap {

    @NotNull
    final IServiceLocator serviceLocator = new ServiceLocator();

    @NotNull
    final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    public Bootstrap() {
    }

    public void run(@Nullable final String[] args){
        try {
            init();
        } catch (DeniedAccessException e) {
            e.printStackTrace();
        } catch (EmptyUserException e) {
            e.printStackTrace();
        } catch (EmptyLoginException e) {
            e.printStackTrace();
        } catch (EmptyHashLineException e) {
            e.printStackTrace();
        } catch (EmptyPasswordException e) {
            e.printStackTrace();
        } catch (BusyLoginException e) {
            e.printStackTrace();
        } catch (EmptyRoleException e) {
            e.printStackTrace();
        }
        System.out.println("** SERVER IS RUNNING ** \n");
    }

    public void init() throws DeniedAccessException, EmptyRoleException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException {
        initProperty();
        initUsers();
        initEndpoint();
    }

    private void initProperty() {
        serviceLocator.getPropertyService().init();
    }

    private void initUsers() throws DeniedAccessException, EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException {
        serviceLocator.getUserService().add("1", "1");
        serviceLocator.getUserService().add("test", "test");
        serviceLocator.getUserService().add("admin", "admin", Role.ADMIN);
    }

    private void initEndpoint() {
        registryEndpoint(endpointLocator.getUserEndpoint());
        registryEndpoint(endpointLocator.getSessionEndpoint());
        registryEndpoint(endpointLocator.getTaskEndpoint());
        registryEndpoint(endpointLocator.getProjectEndpoint());
        registryEndpoint(endpointLocator.getAdminEndpoint());
        registryEndpoint(endpointLocator.getAdminUserEndpoint());
        registryEndpoint(endpointLocator.getAuthEndpoint());
    }

    private void registryEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    public IEndpointLocator getEndpointLocator() {
        return endpointLocator;
    }
}
