package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<User> {

    @Nullable
    User getById(@Nullable String id) throws EmptyIdException;

    @Nullable
    User getByLogin(@Nullable String login) throws EmptyLoginException;

    boolean add(@Nullable String login, @Nullable String password) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyPasswordException;

    boolean add(@Nullable String login, @Nullable String password, @Nullable String firstName) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyFirstNameException, EmptyPasswordException;

    boolean add(@Nullable String login, @Nullable String password, @Nullable Role role) throws DeniedAccessException, EmptyLoginException, EmptyUserException, BusyLoginException, EmptyHashLineException, EmptyRoleException, EmptyPasswordException;

    boolean add(@Nullable final String login, @Nullable final User user) throws BusyLoginException, EmptyUserException, EmptyLoginException;

    @Nullable
    User editProfileById(@Nullable String id, @Nullable String firstName) throws EmptyUserException, EmptyFirstNameException, EmptyIdException;

    @Nullable
    User editProfileById(@Nullable String id, @Nullable String firstName, @Nullable String lastName) throws EmptyUserException, EmptyLastNameException, EmptyFirstNameException, EmptyIdException;

    @Nullable
    User editProfileById(@Nullable String id, @Nullable final String email, @Nullable String firstName, @Nullable String lastName, @Nullable final String middleName) throws EmptyUserException, EmptyMiddleNameException, EmptyLastNameException, EmptyFirstNameException, EmptyEmailException, EmptyIdException;

    @NotNull
    User updatePasswordById(@Nullable String id, @Nullable String newPassword) throws EmptyUserException, IncorrectHashPasswordException, EmptyHashLineException, EmptyNewPasswordException, EmptyIdException;

    @Nullable
    User removeById(@Nullable String id) throws EmptyIdException;

    @Nullable
    User removeByLogin(@Nullable String login) throws EmptyLoginException;

    @Nullable
    User removeByUser(@Nullable User user) throws EmptyUserException;

    @Nullable
    User lockUserByLogin(@Nullable String login) throws EmptyUserException, EmptyLoginException;

    @Nullable
    User unlockUserByLogin(@Nullable String login) throws EmptyUserException, EmptyLoginException;

}
