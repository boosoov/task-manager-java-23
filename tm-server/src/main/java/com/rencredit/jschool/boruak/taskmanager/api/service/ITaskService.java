package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<Task> {

    boolean create(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    boolean create(@Nullable String userId, @Nullable String name, @Nullable String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    boolean create(@Nullable String userId, @Nullable Task task) throws EmptyTaskException, EmptyUserIdException;

    boolean remove(@Nullable String userId, @Nullable Task task) throws EmptyTaskException, EmptyUserIdException;

    @NotNull
    List<Task> findAllByUserId(@Nullable String userId) throws EmptyUserIdException;

    void clearByUserId(@Nullable String userId) throws EmptyUserIdException;

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index) throws EmptyUserIdException, IncorrectIndexException;

    @Nullable
    Task findOneByName(@Nullable String userId, @Nullable String name) throws EmptyUserIdException, EmptyNameException;

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws IncorrectIndexException, EmptyUserIdException;

    @Nullable
    Task removeOneByName(@Nullable String userId, @Nullable String name) throws EmptyNameException, EmptyUserIdException;

    @NotNull
    Task updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyNameException, EmptyDescriptionException;

    @NotNull
    Task updateTaskByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws EmptyTaskException, EmptyNameException, EmptyUserIdException, IncorrectIndexException, EmptyDescriptionException;

}
