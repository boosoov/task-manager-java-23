package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService {

    boolean close(@Nullable final Session session) throws DeniedAccessException;

    void closeAll(@Nullable final Session session) throws DeniedAccessException;

    @Nullable
    User getUser(@Nullable final Session session) throws DeniedAccessException, EmptyIdException;

    @Nullable
    String getUserId(@Nullable final Session session) throws DeniedAccessException;

    @NotNull
    List<Session> getListSession(@Nullable final Session session) throws DeniedAccessException;

    @Nullable
    Session sign(@Nullable final Session session);

    boolean isValid(@Nullable final Session session);

    void validate(@Nullable final Session session) throws DeniedAccessException;

    void validate(@Nullable final Session session, @Nullable final Role role) throws DeniedAccessException, EmptyIdException;

    @NotNull
    Session open(@Nullable final String login, @Nullable final String password) throws DeniedAccessException, EmptyPasswordException, EmptyLoginException, EmptyHashLineException, EmptyUserException;

    boolean checkUserAccess(@NotNull final String login, @NotNull final String password) throws EmptyHashLineException, EmptyLoginException;

}
