package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.Domain;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyElementsException;
import org.jetbrains.annotations.Nullable;

public interface IDomainService {

    void load(@Nullable final Domain domain) throws EmptyElementsException;

    void export(@Nullable final Domain domain);

}
