package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyTaskException extends AbstractException {

    public EmptyTaskException() {
        super("Error! Task not exist...");
    }

}
