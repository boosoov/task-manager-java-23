package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IUserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private IUserService userService;

    public UserEndpoint() {
    }

    public UserEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        userService = serviceLocator.getUserService();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @WebMethod
    public boolean addUserLoginPassword(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.add(login, password);
    }

    @Override
    @WebMethod
    public boolean addUserLoginPasswordFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) throws EmptyPasswordException, EmptyFirstNameException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.add(login, password, firstName);
    }

    @Override
    @WebMethod
    public boolean addUserLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) throws EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.add(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.getById(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User getUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.getByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User editUserProfileByIdFirstName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName
    ) throws EmptyIdException, EmptyUserException, EmptyFirstNameException, DeniedAccessException {
        sessionService.validate(session);
        return userService.editProfileById(id, firstName);
    }

    @Nullable
    @Override
    @WebMethod
    public User editUserProfileByIdFirstNameLastName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName
    ) throws EmptyUserException, EmptyIdException, EmptyFirstNameException, EmptyLastNameException, DeniedAccessException {
        sessionService.validate(session);
        return userService.editProfileById(id, firstName, lastName);
    }

    @Nullable
    @Override
    @WebMethod
    public User editUserProfileByIdFirstNameLastNameMiddleName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "email", partName = "email") String email,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "lastName", partName = "lastName") String lastName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) throws EmptyIdException, EmptyLastNameException, EmptyEmailException, EmptyMiddleNameException, EmptyFirstNameException, EmptyUserException, DeniedAccessException {
        sessionService.validate(session);
        return userService.editProfileById(id, email, firstName, lastName, middleName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserPasswordById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    ) throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException, DeniedAccessException {
        sessionService.validate(session);
        return userService.updatePasswordById(id, newPassword);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, DeniedAccessException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.removeById(id);
    }

    @Nullable
    @Override
    @WebMethod
    public User removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) throws EmptyLoginException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void clearAllUser(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException, EmptyUserException, EmptyLoginException, EmptyPasswordException, BusyLoginException, EmptyHashLineException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        userService.clearAll();
        userService.add("1", "1");
        userService.add("test", "test");
        userService.add("admin", "admin", Role.ADMIN);
    }

    @NotNull
    @Override
    @WebMethod
    public List<User> getUserList(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws DeniedAccessException, EmptyIdException, NotExistUserException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        return userService.getList();
    }

}
