package com.rencredit.jschool.boruak.taskmanager.repository;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final Task task) {
        return remove(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> userTasks = new ArrayList<>();
        for (@NotNull final Task task : getList()) {
            if (userId.equals(task.getUserId())) {
                userTasks.add(task);
            }
        }
        return userTasks;
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        @NotNull List<Task> tasks = getList();
        for (int i = 0; i < tasks.size(); i++) {
            if (userId.equals(tasks.get(i).getUserId())) {
                tasks.remove(tasks.get(i));
                i--;
            }
        }
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        for (@NotNull final Task task : getList()) {
            if (userId.equals(task.getUserId()) && id.equals(task.getId())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<Task> userTasks = new ArrayList<>();
        for (Task task : getList()) {
            if (userId.equals(task.getUserId())) userTasks.add(task);
        }
        if (userTasks.size() < index) return null;
        return userTasks.get(index);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (@NotNull final Task task : getList()) {
            if (userId.equals(task.getUserId()) &&
                    name.equals(task.getName())) return task;
        }
        return null;
    }

    @Nullable
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

}
